#! /usr/bin/env python
# -*- coding: utf-8 -*-

# Copyright (C) 2009-2010 Adrien Grand
#
# This file is part of VLMa.
#
# VLMa is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# VLMa is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with VLMa. If not, see <http://www.gnu.org/licenses/>.


import conf, re
from streamer.api import *

NUMERIC_RE = re.compile("\d+")
IPV4_RE    = re.compile("(\d+)\.(\d+)\.(\d+)\.(\d+)")
IPV6_RE    = re.compile("([A-Fa-f0-9]*[:]{1,2})+[A-Fa-f0-9]*")
ID_RE      = re.compile("[A-Za-z0-9]+")

class ValidationException(Exception):

  def __init__(self, message=None):
    Exception.__init__(self, message)


def validate_order(order):
  # Validate the id
  match = ID_RE.match(order.id)
  if match is None or match.group() != order.id:
    raise ValidationException("the id of the order is not valid: '%s'" %order.id)

  # Check whether the streamer exists
  if not order.streamer is None:
    for streamer in conf.STREAMERS:
      if streamer.id == order.streamer:
        break
    raise ValidationException("No such streamer: '%s'" %order.streamer)

  for src, dests in order.programs.items():
    if isinstance(order, DVBOrder) and NUMERIC_RE.match(src) is None:
      # DVB channels have a SID as src which is an integer formatted as a string
      raise ValidationException("'src' has to be an integer for DVB channels: '%s' is not valid" %src)
    for dest in dests:
      validate_dest(order, dest)


def validate_dest(order, dest):
  # Check IP
  match_v4 = IPV4_RE.match(dest.ip)
  match_v6 = IPV6_RE.match(dest.ip)
  if match_v4 is None and match_v6 is None:
    raise ValidationException("'%s' is not a valid IP address" %dest.ip)
  elif not match_v4 is None:
    # TODO: more checks
    if match_v4.group() != dest.ip:
      raise ValidationException("'%s' is not a valid IPv4 address" %dest.ip)
  else:
    # TODO: more checks
    if match_v6.group() != dest.ip:
      raise ValidationException("'%s' is not a valid IPv6 address" %dest.ip)

  check_streaming(order, dest, dest.streaming)
  if not dest.transcoding is None:
    check_transcoding(order, dest, dest.transcoding)
  if not dest.announcing is None:
    check_announcing(order, dest, dest.announcing)


def check_streaming(order, dest, streaming):
  # Check the type
  supported_types = ["broadcast"]
  if isinstance(order, FilesOrder):
    supported_types.append("vod")
  if not streaming.type in supported_types:
    raise ValidationException("'%s' is not a valid streaming type" %streaming.type)

  # Check the protocol
  if streaming.type == "broadcast":
    supported_protocols = ["udp", "rtp", "http"]
    if not streaming.protocol in supported_protocols:
      raise ValidationException("'%s' protocol is not supported for broadcasts" %streaming.protocol)
  else:
    if streaming.protocol != "rtsp":
      raise ValidationException("'%s' protocol is not supported for VoD" %streaming.protocol)

  # Check the encapsulation
  supported_mux = ["ts", "ogg", "asf", "raw"]
  if streaming.mux not in supported_mux:
    raise ValidationException("'%s' mux is not supported" %streaming.mux)


def check_transcoding(order, dest, transcoding):
  # Check the video codec
  supported_vcodecs = ["mp1v", "mp2v", "mp4v", "h264"]
  if not transcoding.vcodec in supported_vcodecs:
    raise ValidationException("video codec '%s' is not supported" %transcoding.vcodec)

  # Check the audio codec
  supported_vcodecs = ["mpga", "a52", "mp3", "mp4a"]
  if not transcoding.vcodec in supported_vcodecs:
    raise ValidationException("audio codec '%s' is not supported" %transcoding.acodec)


def check_announcing(order, dest, annoucing):
  # Check the type
  supported_types = ["sap"]
  if not annoucing.type in supported_types:
    raise ValidationException("annoucing type '%s' is not supported" %annoncing.type)

  if announcing.type == "sap" and dest.streaming.protocol not in ["udp", "rtp"]:
    raise ValidationException("SAP announces can only be used with multicast streaming, %s found" %dest.streaming.protocol)


