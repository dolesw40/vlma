#! /usr/bin/env python
# -*- coding: utf-8 -*-

# Copyright (C) 2009-2010 Adrien Grand
#
# This file is part of VLMa.
#
# VLMa is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# VLMa is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with VLMa. If not, see <http://www.gnu.org/licenses/>.


import logging, math, threading, socket, struct, time

MIME_TYPE = "application/sdp"
SAP_PORT  = 9875


class Announce:

  def __init__(self, orig, dest, version):
    self.ip_orig = orig
    self.ip = self.__guess_ip(dest.ip)
    self.version = version
    self.sdp = self.__build_sdp(dest)

  def __guess_ip(self, multicast_address):
    # TODO
    return "239.255.255.255"

  def __build_sdp(self, dest):
    is_ipv6 = self.ip.find(':') > 0
    ip_version = is_ipv6 and "IP6" or "IP4"
    now = int(math.floor(time.time() * 1000L))
    # cf. http://www.ietf.org/rfc/rfc2327.txt
    # v, o, s, t and m are compulsory
    result = ["v=0"]
    result.append("o=VideoLAN %ld 1 IN %s %s" %(now, ip_version, self.ip_orig))
    result.append("s=%s" %dest.announcing.name)
    if not dest.announcing.description is None:
      result.append("i=%s" %dest.announcing.description)
    result.append("c=IN %s %s%s" %(ip_version, dest.ip, is_ipv6 and "" or "/%d" %dest.ttl))
    result.append("t=0 0")
    result.append("a=tool:VLMa")
    result.append("a=type:broadcast")
    result.append("a=charset:UTF-8")
    result.append("a=recvonly") # No interaction required, default for type:broadcast
    if not dest.announcing.group is None:
      result.append("a=x-plgroup:%s" %dest.announcing.group)
    if dest.streaming.type == "rtp":
      result.append("m=video %d RTP/AVP 33" %dest.port)
    else:
      result.append("m=video %d udp mpeg" %dest.port)
    return "\r\n".join(result)


class SAPServer(threading.Thread):
  """A SAP server"""

  def __init__(self, ip, delay=5):
    """Create a new SAP server"""
    threading.Thread.__init__(self)
    self.ip = ip
    self.delay = delay
    self.__announces = {}
    self.__lock = threading.RLock()
    self.__logger = logging.getLogger("SAP server")
    self.setDaemon(True)

  def add_dest(self, dest):
    version = int(math.floor(time.time() * 1000)) & 0xffff
    self.__lock.acquire()
    self.__announces[dest] = Announce(self.ip, dest, version)
    self.__lock.release()

  def remove_dest(self, dest):
    self.__lock.acquire()
    if self.__announces.has_key(dest):
      del self.__announces[dest]
    self.__lock.release()

  def remove_dests(self):
    self.__lock.acquire()
    self.__announces.clear()
    self.__lock.release()

  def __send_announce(self, announce):
    is_ipv6 = self.ip.find(":") > 0
    ip = socket.inet_pton(is_ipv6 and socket.AF_INET6 or socket.AF_INET, self.ip)
    data = struct.pack("!2BH%ds%ds%ds" %(len(ip), len(MIME_TYPE), len(announce.sdp)),
        # SAP Header
        0x20 | (is_ipv6 and 0x10 or 0x00), # SAP version + IP version
        0x00, # Authentication length (none)
        announce.version, # a version hash, must be changed whenever the content of the announce is changed
        ip,
        MIME_TYPE,
        # Session description
        announce.sdp) # content
    self.__sock.sendto(data, (announce.ip, SAP_PORT))

  def run(self):
    self.__sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
    self.__sock.setsockopt(socket.IPPROTO_IP, socket.IP_MULTICAST_TTL, 2)
    while True:
      self.__lock.acquire()
      for announce in self.__announces.values():
        try:
          self.__send_announce(announce)
        except BaseException, e:
          self.__logger.warn("Error while sending SAP announce", e)
          print e
      self.__lock.release()
      time.sleep(self.delay)
    self.__sock.close()

