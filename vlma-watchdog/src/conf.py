#! /usr/bin/env python
# -*- coding: utf-8 -*-

# Copyright (C) 2009-2010 Adrien Grand
#
# This file is part of VLMa.
#
# VLMa is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# VLMa is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with VLMa. If not, see <http://www.gnu.org/licenses/>.


import streamer, logging

NETWORK_ADDRESS = "127.0.0.1"
# Not taken into account on Windows
NETWORK_INTERFACE = "eth0"

# HTTP interface exposed by this script
SERVER_PORT = 4213
SERVER_LOGIN = "videolan"
SERVER_PASSWORD = "admin"

STREAMERS = [
#  streamer.vlc.VLC("vlc", "vlc", {streamer.vlc.VLCOption.TELNET_PORT: 4214}),
#  streamer.vlc.VLC("vlc2", "vlc", {streamer.vlc.VLCOption.TELNET_PORT: 4215}),
  streamer.dvblast.DVBlast("dvblast", "/home/jpountz/src/dvblast/dvblast")
]

