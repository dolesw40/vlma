#! /usr/bin/env python
# -*- coding: utf-8 -*-

# Copyright (C) 2009-2010 Adrien Grand
#
# This file is part of VLMa.
#
# VLMa is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# VLMa is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with VLMa. If not, see <http://www.gnu.org/licenses/>.


import logging, signal, time

# The log level
LOG_LEVEL = logging.DEBUG

if __name__ == '__main__':
  # Init logging
  logger = logging.getLogger()
  console = logging.StreamHandler()
  formatter = logging.Formatter("%(asctime)s [%(levelname)s] %(message)s")
  console.setFormatter(formatter)
  logger.addHandler(console)
  logger.setLevel(LOG_LEVEL)

  # Import modules
  conf  = __import__("conf")
  utils = __import__("utils")
  web   = __import__("web")

  # Signal handling
  signal.signal(signal.SIGINT, utils.signalHandler)
  signal.signal(signal.SIGTERM, utils.signalHandler)
  if not utils.platform_is_windows:
    signal.signal(signal.SIGHUP, utils.signalHandler)
    signal.signal(signal.SIGQUIT, utils.signalHandler)

  if not utils.check_port(conf.SERVER_PORT):
    logger.error("Cannot listen on port %d (%s), port is already in use",
        conf.SERVER_PORT, "Watchdog HTTP interface")
    sys.exit(1)
  # Start web server
  webserver = web.Server(conf.STREAMERS)
  webserver.start()
  # Main thread
  while True:
    time.sleep(1)

