#! /usr/bin/env python
# -*- coding: utf-8 -*-

# Copyright (C) 2009-2010 Adrien Grand
#
# This file is part of VLMa.
#
# VLMa is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# VLMa is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with VLMa. If not, see <http://www.gnu.org/licenses/>.


import streamer
import xml.dom.minidom as minidom

class SerializationException(Exception):

  def __init__(self, message=None):
    Exception.__init__(self, message)


def streamer_to_xml(streamer, writer):
  writer.write("<streamer><id>")
  writer.write(str(streamer.id))
  writer.write("</id><type>")
  writer.write(streamer.type)
  writer.write("</type><version>")
  writer.write(str(streamer.version))
  writer.write("</version><uptime>")
  writer.write("%d" %streamer.uptime)
  writer.write("</uptime></streamer>")

def streamer_metrics_to_xml(streamer, writer):
  writer.write("<streamer-metrics><cpu>")
  writer.write("%f" %streamer.cpu)
  writer.write("</cpu><mem>")
  writer.write("%f" %streamer.mem)
  writer.write("</mem></streamer-metrics>")

def streamer_logs_to_xml(logs, writer):
  writer.write("<logs>")
  for log in logs:
    writer.write("<log level=\"%s\">%s</log>" %(log[0], log[1]))
  writer.write("</logs>")

def system_metrics_to_xml(metrics, writer):
  writer.write("<system-metrics>")
  writer.write("<cpu-load>%f</cpu-load>" %metrics.cpu_load)
  writer.write("<inbound-traffic>%f</inbound-traffic>" %metrics.traffic_in)
  writer.write("<outbound-traffic>%f</outbound-traffic>" %metrics.traffic_out)
  writer.write("</system-metrics>")


def order_from_xml(s):
  xml = minidom.parseString(s).childNodes[0]
  order_type = xml.nodeName
  order = None
  if order_type == "dvbt-order":
    order = streamer.api.DVBTOrder(None)
  elif order_type == "dvbs-order":
    order = streamer.api.DVBSOrder(None)
  elif order_type == "files-order":
    order = streamer.api.FilesOrder(None)
  elif order_type == "stream-order":
    order = streamer.api.StreamOrder(None)
  else:
    raise SerializationException("Unknwon order type: " + order_type)

  for node in xml.childNodes:
    if node.nodeType == xml.TEXT_NODE:
      continue
    nodeName = node.nodeName
    nodeValue = node.childNodes[0].nodeValue
    if nodeName in ["id", "streamer"]:
      setattr(order, nodeName, nodeValue)
    elif nodeName in ["ttl", "adapter", "frequency", "fec", "voltage", "srate", "dvb-bandwidth"]:
      setattr(order, nodeName, int(nodeValue))
    elif nodeName == "programs":
      for program in node.childNodes:
        if program.nodeType == xml.TEXT_NODE:
          continue
        if program.nodeName == "program":
          src = None
          dests = []
          for program_content in program.childNodes:
            if program_content.nodeType == xml.TEXT_NODE:
              continue
            nodeName = program_content.nodeName
            if nodeName == "src":
              src = program_content.childNodes[0].nodeValue
            elif nodeName == "dest":
              dest = streamer.api.Dest(order)
              for dest_content in program_content.childNodes:
                if dest_content.nodeType == xml.TEXT_NODE:
                  continue
                nodeName = dest_content.nodeName
                if nodeName == "ip":
                  dest.ip = dest_content.childNodes[0].nodeValue
                elif nodeName == "port":
                  dest.port = int(dest_content.childNodes[0].nodeValue)
                elif nodeName == "streaming":
                  streaming = streamer.api.Streaming()
                  for streaming_content in dest_content.childNodes:
                    if streaming_content.nodeType == xml.TEXT_NODE:
                      continue
                    nodeName = streaming_content.nodeName
                    nodeValue = streaming_content.childNodes[0].nodeValue
                    if nodeName in ["type", "protocol", "mux"]:
                      setattr(streaming, nodeName, nodeValue)
                    elif nodeName == "loop":
                      streaming.loop = (nodeValue == "true")
                    else:
                      raise SerializationException("Did not expect " + nodeName + " under the 'streaming' node")
                  dest.streaming = streaming
                elif nodeName == "transcoding":
                  transcoding = Transcoding()
                  for transcoding_content in dest_content.childNodes:
                    if transcoding_content.nodeType == xml.TEXT_NODE:
                      continue
                    nodeName = transcoding_content.nodeName
                    nodeValue = transcoding_content.childNodes[0].nodeValue
                    if nodeName in ["vcodec", "acodec"]:
                      setattr(transcoding, nodeName, nodeValue)
                    elif nodeName in ["ab", "vb"]:
                      setattr(transcoding, nodeName, int(nodeValue))
                    elif nodeName == "scale":
                      transcoding.scale = float(nodeValue)
                    elif nodeName == "deinterlace":
                      transcoding.deinterlace = (nodeValue == "true")
                    else:
                      raise SerializationException("Did not expect " + nodeName + " under the 'transcoding' node")
                  dest.transcoding = transcoding
                elif nodeName == "announcing":
                  announcing = Announcing()
                  for announcing_content in dest_content.childNodes:
                    if announcing_content.nodeType == xml.TEXT_NODE:
                      continue
                    nodeName = announcing_content.nodeName
                    nodeValue = announcing_content.childNodes[0].nodeValue
                    if nodeName in ["type", "name", "group"]:
                      setattr(announcing, nodeName, nodeValue)
                    else:
                      raise SerializationException("Did not expect " + nodeName + " under the 'announcing' node")
                  dest.announcing = announcing
                else:
                  raise SerializationException("Did not expect " + nodeName + " under the 'dest' node")
                dests.append(dest)
            else:
              raise SerializationException("Did not expect " + nodeName + " under the 'program' node")
            order.programs[src] = dests
        else:
          raise SerializationException("Did not expect " + program.nodeName + " under the 'programs' node")
    else:
      raise SerializationException("Did not expect " + nodeName + " under the '" + order_type + "' node")
  return order


def order_to_xml(order, writer):
  order_type = None
  if isinstance(order, streamer.api.DVBTOrder):
    order_type = "dvbt"
  elif isinstance(order, streamer.api.DVBSOrder):
    order_type = "dvbs"
  elif isinstance(order, streamer.api.FilesOrder):
    order_type = "files"
  elif isinstance(order, streamer.api.StreamOrder):
    order_type = "stream"
  else:
    raise SerializationException("Unknwon class: " + str(order.__class__))

  writer.write("<%s-order>" %order_type)
  writer.write("<id>%s</id>" %str(order.id))
  if not order.streamer is None:
    writer.write("<streamer>%s</streamer>" %order.streamer)
  writer.write("<ttl>%d</ttl>" %order.ttl)
  if isinstance(order, streamer.api.DVBOrder):
    writer.write("<adapter>%d</adapter>" %order.adapter)
    writer.write("<frequency>%d</frequency>" %order.frequency)
  if isinstance(order, streamer.api.DVBSOrder):
    writer.write("<fec>%d</fec>" %order.fec)
    writer.write("<voltage>%d</voltage>" %order.voltage)
    writer.write("<srate>%d</srate>" %order.srate)
    writer.write("<dvb-bandwidth>%d</dvb-bandwidth>" %order.bandwidth)
  writer.write("<programs>")
  for src, dests in order.programs.items():
    writer.write("<program>")
    writer.write("<src>%s</src>" %str(src))
    for dest in dests:
      writer.write("<dest>")
      writer.write("<ip>%s</ip>" %dest.ip)
      writer.write("<port>%s</port>" %dest.port)
      writer.write("<streaming>")
      writer.write("<type>%s</type>" %dest.streaming.type)
      writer.write("<protocol>%s</protocol>" %dest.streaming.protocol)
      writer.write("<mux>%s</mux>" %dest.streaming.mux)
      if isinstance(order, streamer.api.FilesOrder):
        writer.write("<loop>%s</loop>" %(dest.streaming.loop and "true" or "false"))
      writer.write("</streaming>")
      if not dest.transcoding is None:
        writer.write("<transcoding>")
        writer.write("<vcodec>%s</vcodec>" %dest.transcoding.vcodec)
        writer.write("<acodec>%s</acodec>" %dest.transcoding.acodec)
        writer.write("<vb>%d</vb>" %dest.transcoding.vb)
        writer.write("<scale>%f</scale>" %dest.transcoding.scale)
        writer.write("<ab>%d</ab>" %dest.transcoding.ab)
        writer.write("<deinterlace>%s</deinterlace>" %(dest.transcoding.deinterlace and "true" or "false"))
        writer.write("</transcoding>")
      writer.write("</dest>")
    writer.write("</program>")
  writer.write("</programs>")
  writer.write("</%s-order>" %order_type)

def error_to_xml(error, writer):
  writer.write("<error><code>%s</code><message>%s</message></error>" %(error.code.name, error.msg))

