/*
 * Copyright (C) 2008 the VideoLAN team
 *
 * This file is part of VLMa.
 *
 * VLMa is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * VLMa is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VLMa. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.videolan.vlma.order;

import static org.junit.Assert.assertEquals;

import java.net.UnknownHostException;

import org.apache.commons.configuration.BaseConfiguration;
import org.apache.commons.configuration.Configuration;
import org.junit.Before;
import org.junit.Test;
import org.videolan.vlma.model.Command;
import org.videolan.vlma.model.Server;
import org.videolan.vlma.order.sender.CommandLogger;

public class CommandLoggerTest {

    private Configuration configuration;
    private CommandLogger commandLogger;
    private Server server;

    @Before
    public void setup() throws UnknownHostException {
        configuration = new BaseConfiguration();
        configuration.setProperty("vlma.ui.command.queue.size", 50);
        commandLogger = new CommandLogger();
        commandLogger.setConfiguration(configuration);
        server = new Server();
    }

    @Test
    public void testAdd() {
        for (int i = 1; i < 100; i++) {
            commandLogger.add(new Command(server, "command " + i, "response " + i));
        }
        assertEquals(50, commandLogger.getCommands().size());
        configuration.setProperty("vlma.ui.command.queue.size", 25);
        commandLogger.add(new Command(server, "command", "response"));
        assertEquals(commandLogger.getCommands().size(), 25);
    }

}
