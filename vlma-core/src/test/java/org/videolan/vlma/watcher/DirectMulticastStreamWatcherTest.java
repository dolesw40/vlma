/*
 * Copyright (C) 2008 the VideoLAN team
 *
 * This file is part of VLMa.
 *
 * VLMa is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * VLMa is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VLMa. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.videolan.vlma.watcher;

import static org.junit.Assert.assertFalse;

import java.net.InetAddress;

import org.junit.Before;
import org.junit.Test;
import org.videolan.vlma.model.Program;
import org.videolan.vlma.order.watcher.DirectMulticastStreamWatcher;
import org.videolan.vlma.order.watcher.StreamWatcher;

public class DirectMulticastStreamWatcherTest {

    // No streaming on this address since it is not a multicast one
    static final String localMulticastAddress = "127.0.0.1";

    private StreamWatcher watcher;

    @Before
    public void setup() {
        watcher = new DirectMulticastStreamWatcher();
    }

    @Test
    public void testIsPlayed() throws Exception {
        Program program = new Program();
        // Pick a random address
        program.setIp(InetAddress.getByName(localMulticastAddress));
        assertFalse(watcher.isPlayed(program));
    }

}
