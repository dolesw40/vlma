/*
 * Copyright (C) 2008 the VideoLAN team
 *
 * This file is part of VLMa.
 *
 * VLMa is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * VLMa is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VLMa. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.videolan.vlma;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.apache.commons.configuration.BaseConfiguration;
import org.apache.commons.configuration.Configuration;
import org.junit.Before;
import org.junit.Test;
import org.videolan.vlma.model.AnnouncingStrategy;
import org.videolan.vlma.model.Program;
import org.videolan.vlma.model.StreamingStrategy;
import org.videolan.vlma.util.ProgramFactory;

public class ProgramFactoryTest {

    private Configuration configuration;
    private ProgramFactory factory;

    @Before
    public void setUp() {
        configuration = new BaseConfiguration();
        configuration.setProperty("vlma.announcement", "PODCAST");
        configuration.setProperty("vlma.streaming", "HTTP");
        configuration.setProperty("vlma.encapsulation", "TS");
        factory = new ProgramFactory();
        factory.setConfiguration(configuration);
    }

    @Test
    public void testGetProgram() {
        Program program = factory.newProgram();
        assertNotNull(program);
        assertEquals(program.getStreamingStrategy().getProtocol(), StreamingStrategy.Protocol.HTTP);
        assertEquals(program.getStreamingStrategy().getEncapsulation(), StreamingStrategy.Encapsulation.TS);
        assertEquals(program.getAnnouncingStrategy().getAnnouncements().size(), 1);
        assertTrue(program.getAnnouncingStrategy().getAnnouncements().contains(AnnouncingStrategy.Announcement.PODCAST));
    }

    @Test
    public void testSeveralAnnouncements() {
        configuration.setProperty("vlma.announcement", "PODCAST, SAP");
        Program program = factory.newProgram();
        assertEquals(program.getAnnouncingStrategy().getAnnouncements().size(), 2);
        assertTrue(program.getAnnouncingStrategy().getAnnouncements().contains(AnnouncingStrategy.Announcement.PODCAST));
        assertTrue(program.getAnnouncingStrategy().getAnnouncements().contains(AnnouncingStrategy.Announcement.SAP));
    }

}
