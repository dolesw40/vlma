/*
 * Copyright (C) 2006-2008 the VideoLAN team
 *
 * This file is part of VLMa.
 *
 * VLMa is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * VLMa is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VLMa. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.videolan.vlma.daemon;

import java.net.UnknownHostException;
import java.rmi.RemoteException;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import org.apache.log4j.Logger;
import org.springframework.remoting.rmi.RmiServiceExporter;
import org.videolan.vlma.IpBank;
import org.videolan.vlma.dao.VLMaDao;
import org.videolan.vlma.monitor.Monitor;
import org.videolan.vlma.notifier.Notifier;

/**
 * The VLMa daemon.
 *
 * @author Adrien Grand <jpountz at videolan.org>
 */
public class Daemon {

    private static final Logger logger = Logger.getLogger(Daemon.class);
    private static final Daemon INSTANCE = new Daemon();

    private IpBank ipBank;
    private List<Monitor> monitors;
    private List<Notifier> notifiers;
    private RmiServiceExporter rmiServiceExporter;
    private VLMaDao vlmaDao;
    private AtomicBoolean running;

    private Daemon() {
        running = new AtomicBoolean(true);
    }

    public static Daemon getInstance() {
        return INSTANCE;
    }

    public boolean isRunning() {
        return running.get();
    }

    /**
     * Sets the service exporter.
     *
     * @param rmiServiceExporter the rmiServiceExporter to set
     */
    public synchronized void setRmiServiceExporter(RmiServiceExporter rmiServiceExporter) {
        this.rmiServiceExporter = rmiServiceExporter;
    }

    /**
     * Sets the IP bank.
     *
     * @param ipBank the IP bank to set
     */
    public synchronized void setIpBank(IpBank ipBank) {
        this.ipBank = ipBank;
    }

    /**
     * Sets the monitors.
     *
     * @param monitors the monitors to set
     */
    public synchronized void setMonitors(List<Monitor> monitors) {
        this.monitors = monitors;
    }

    /**
     * Sets the notifiers.
     *
     * @param notifiers the notifiers to set
     */
    public synchronized void setNotifiers(List<Notifier> notifiers) {
        this.notifiers = notifiers;
    }

    /**
     * Sets the VLMa DAO.
     *
     * @param vlmaDao the vlmaDao to set
     */
    public synchronized void setVlmaDao(VLMaDao vlmaDao) {
        this.vlmaDao = vlmaDao;
    }

    /**
     * Starts the daemon.
     */
    synchronized public void start() {
        // First, load data from disk
        vlmaDao.loadFromDisk();

        // Then initialize the ipBank,
        try {
            ipBank.initIps();
        } catch (UnknownHostException e) {
            // TODO Do better?
            logger.error(e);
        }

        // the notifiers,
        for(Notifier notifier : notifiers) {
            notifier.init();
        }

        // and start the monitors
        for(Monitor monitor : monitors) {
            monitor.start();
        }
    }

    synchronized public void reload() {
        vlmaDao.loadFromDisk();
    }

    synchronized public void stop() {
        running.set(false);

        // Stop the monitors
        logger.info("Stopping monitors...");
        for (Monitor monitor : monitors) {
            monitor.stop();
        }

        // Destroy the notifiers
        logger.info("Stopping notifiers...");
        for(Notifier notifier : notifiers) {
            notifier.destroy();
        }

        // Stop the RMI exporter
        try {
            rmiServiceExporter.destroy();
        } catch (RemoteException e) {
            logger.error("Unable to stop the RMI exporter", e);
        }
    }

}
