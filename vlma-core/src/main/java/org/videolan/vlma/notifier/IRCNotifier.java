/*
 * Copyright (C) 2008-2009 the VideoLAN team
 *
 * This file is part of VLMa.
 *
 * VLMa is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * VLMa is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VLMa. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.videolan.vlma.notifier;

import java.io.IOException;
import java.util.List;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.event.ConfigurationEvent;
import org.apache.commons.configuration.event.ConfigurationListener;
import org.apache.log4j.Logger;
import org.schwering.irc.lib.IRCConnection;
import org.schwering.irc.lib.IRCConstants;
import org.schwering.irc.lib.IRCEventListener;
import org.schwering.irc.lib.IRCModeParser;
import org.schwering.irc.lib.IRCUser;

/**
 * An IRC notifier.
 *
 * @author Adrien Grand <jpountz at videolan.org>
 */
public class IRCNotifier extends Notifier implements ConfigurationListener {

    private static final Logger logger = Logger.getLogger(IRCNotifier.class);

    private static final String NICK = "vlma.notification.irc.nick";
    private static final String PASS = "vlma.notification.irc.pass";
    private static final String HOST = "vlma.notification.irc.host";
    private static final String PORT = "vlma.notification.irc.port";
    private static final String CHANS = "vlma.notification.irc.chan";

    private Configuration configuration;
    private IRCConnection conn;
    private String nick;
    private String pass;
    private List<String> chans;
    private String host;
    private int port = -1;
    private boolean configurationChanged = false;

    /* (non-Javadoc)
     * @see org.apache.commons.configuration.event.ConfigurationListener#configurationChanged(org.apache.commons.configuration.event.ConfigurationEvent)
     */
    public void configurationChanged(ConfigurationEvent event) {
        if (!event.isBeforeUpdate()) {
            String key = event.getPropertyName();
            Object value = event.getPropertyValue();
            synchronized (this) {
                if (nick != null && NICK.equals(key) && !nick.equals(value) ||
                        pass != null && PASS.equals(key) && !pass.equals(value) ||
                        host != null && HOST.equals(key) && !host.equals(value) ||
                        PORT.equals(key) && !value.equals(port) ||
                        chans != null && CHANS.equals(key) && !chans.equals(value)) {
                    logger.info("Configuration of the IRC notifier changed, will reconnect");
                    configurationChanged = true;
                }
            }
        }
    }

    /**
     * Connect to the IRC server and join the appropriate channel.
     *
     * @throws IOException
     */
    @SuppressWarnings("unchecked")
    private void connect() throws IOException {
        synchronized (this) {
            nick = configuration.getString(NICK);
            pass = configuration.getString(PASS);
            host = configuration.getString(HOST);
            port = configuration.getInt(PORT);
            chans = configuration.getList(CHANS);
            conn = new IRCConnection(host, new int[] { port }, pass, nick, "vlma", "VLMa");
            conn.setEncoding("UTF-8");
            conn.addIRCEventListener(new Listener());
            conn.setPong(true);
            conn.setDaemon(true);
            conn.connect();
        }
        /* Wait for 5 seconds before trying to join any chan.
         * It may be necessary if the server is slow to initialise the connection.
         */
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) { }
        synchronized (this) {
            for (String chan : chans) {
                conn.doJoin(chan);
            }
        }
    }

    @Override
    public void init() {
        if(isIRCEnabled()) {
            try {
                connect();
            } catch (IOException e) {
                logger.error("Error while connecting", e);
            }
        }
        register(this);
    }

    /**
     * Reconnect to the IRC server if the connection has been broken.
     *
     * @throws IOException
     */
    private synchronized void checkConnection() throws IOException {
        if (conn != null && configurationChanged) {
            if (conn.isConnected()) {
                conn.doQuit("Configuration changed.");
            }
            conn.close();
        }
        if (conn == null || !conn.isConnected()) {
            connect();
        }
    }

    /**
     * Does the user want to be notified by IRC?
     *
     * @return true if and only if there is at least one channel provided in the configuration
     */
    @SuppressWarnings("unchecked")
    private boolean isIRCEnabled() {
        List<String> tmp = configuration.getList(CHANS);
        return (tmp != null && tmp.size() > 0);
    }

    @Override
    public void sendNotification(String message) {
        if (isIRCEnabled()) {
            synchronized (this) {
                for (String chan : chans) {
                    try {
                        checkConnection();
                        conn.doPrivmsg(chan, message);
                    } catch (IOException e) {
                        logger.error("IRC notifier cannot connect", e);
                    }
                }
            }
        }
    }

    @Override
    public void destroy() {
        unregister(this);
        if(conn != null) {
            if (conn.isConnected()) {
                conn.doQuit("VLMa daemon stopping. Quitting IRC.");
            }
            conn.close();
        }
    }

    /**
     * Sets the configuration.
     *
     * @param configuration the configuration to set
     */
    public void setConfiguration(Configuration configuration) {
        this.configuration = configuration;
    }

    /**
     * A listener for the {@link IRCNotifier} class.
     *
     * @author Adrien Grand <jpountz at videolan.org>
     */
    public class Listener implements IRCEventListener {

        public void onDisconnected() {
            logger.info("IRC notifier deconnected");
        }

        public void onError(String error) {
            logger.error("IRC notifier encountered an error: " + error);
        }

        public void onError(int code, String error) {
            logger.error("IRC notifier encountered an error: " + error);
            if (code == IRCConstants.ERR_NICKNAMEINUSE) {
                // Reconnect with another nickname
                synchronized (IRCNotifier.this) {
                    nick = nick + "_";
                    conn.doNick(nick);
                    try {
                        conn.connect();
                    } catch (IOException e) {
                        logger.error("Cannot connect to the IRC server", e);
                    }
                }
            }
        }

        public void onInvite(String chan, IRCUser user, String nick) { }

        public void onJoin(String chan, IRCUser user) {
            synchronized (IRCNotifier.this) {
                logger.info("IRC notifier joined " + chan);
            }
        }

        public void onKick(String chan, IRCUser user, String nick, String msg) {
            logger.info("IRC notifier was kicked by " + user.getNick());
        }

        public void onMode(String arg0, IRCUser arg1, IRCModeParser arg2) { }

        public void onMode(IRCUser arg0, String arg1, String arg2) { }

        public void onNick(IRCUser arg0, String arg1) { }

        public void onNotice(String arg0, IRCUser arg1, String arg2) { }

        public void onPart(String arg0, IRCUser arg1, String arg2) { }

        public void onPing(String ping) {
            logger.info("IRC notifier received ping " + ping);
        }

        public void onPrivmsg(String chan, IRCUser user, String msg) {
            logger.debug("User " + user + " said " + msg);
        }

        public void onQuit(IRCUser arg0, String arg1) { }

        public void onRegistered() { }

        public void onReply(int arg0, String arg1, String arg2) { }

        public void onTopic(String arg0, IRCUser arg1, String arg2) { }

        public void unknown(String arg0, String arg1, String arg2, String arg3) { }
    }

}
