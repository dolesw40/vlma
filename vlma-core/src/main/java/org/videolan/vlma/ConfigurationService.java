/*
 * Copyright (C) 2006-2008 the VideoLAN team
 *
 * This file is part of VLMa.
 *
 * VLMa is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * VLMa is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VLMa. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.videolan.vlma;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.configuration.AbstractConfiguration;
import org.apache.commons.configuration.Configuration;

/**
 * The VLMa configuration.
 * This implementation of <code>Configuration</code> searchs for properties in
 * the following order:
 *   - in SystemConfiguration (values given to the Java VM using the -D option)
 *   - in the user configuration,
 *   - in the default configuration (embedded in the JAR).
 * When a proprerty is added to this configuration, it is added to the user
 * configuration.
 *
 * @author Adrien Grand <jpountz at videolan.org>
 */
public class ConfigurationService extends AbstractConfiguration {

    private Configuration userConfiguration;
    private Configuration systemConfiguration;
    private Configuration defaultConfiguration;

    public void setDefaultConfiguration(Configuration defaultConfiguration) {
        this.defaultConfiguration = defaultConfiguration;
    }

    public void setSystemConfiguration(Configuration systemConfiguration) {
        this.systemConfiguration = systemConfiguration;
    }

    public void setUserConfiguration(Configuration userConfiguration) {
        this.userConfiguration = userConfiguration;
    }

    @Override
    protected void addPropertyDirect(String key, Object value) {
        userConfiguration.addProperty(key, value);
    }

    @Override
    protected void clearPropertyDirect(String key) {
        userConfiguration.clearProperty(key);
    }

    @Override
    public boolean containsKey(String key) {
        return systemConfiguration.containsKey(key)
            && userConfiguration.containsKey(key)
            && defaultConfiguration.containsKey(key);
    }

    @Override
    public Iterator<?> getKeys() {
        List<String> keys = new ArrayList<String>();
        for (Iterator<?> it = userConfiguration.getKeys(); it.hasNext();) {
            keys.add((String)it.next());
        }
        for (Iterator<?> it = defaultConfiguration.getKeys(); it.hasNext();) {
            String key = (String)it.next();
            if (!keys.contains(key)) {
                keys.add(key);
            }
        }
        return keys.iterator();
    }

    @Override
    public Object getProperty(String key) {
        Object result = systemConfiguration.getProperty(key);
        if (result == null) {
            result = userConfiguration.getProperty(key);
            if (result == null) {
                result = defaultConfiguration.getProperty(key);
            }
        }
        return result;
    }

    @Override
    public boolean isEmpty() {
        return userConfiguration.isEmpty()
            && defaultConfiguration.isEmpty();
    }

}
