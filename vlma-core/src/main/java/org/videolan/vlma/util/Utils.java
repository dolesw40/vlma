/*
 * Copyright (C) 2008-2009 the VideoLAN team
 *
 * This file is part of VLMa.
 *
 * VLMa is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * VLMa is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VLMa. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.videolan.vlma.util;

import org.apache.commons.configuration.Configuration;
import org.videolan.vlma.model.Program;

public class Utils {

    private Configuration configuration;

    public String getUrl(Program program) {
        switch (program.getStreamingStrategy().getProtocol()) {
        case HTTP:
            return "http://" + program.getPlayer().getAddress() + ":" + configuration.getInt("vlma.streaming.http.port") + "/" + program.getId();
        case UDP_MULTICAST:
            return "udp://@" + program.getIp().getHostAddress();
        case RTSP:
            int rtspPort = configuration.getInt("vlma.streaming.rtsp.port");
            return "rtsp://" + program.getPlayer().getAddress() + ":"
                    + rtspPort + "/" + program.getId();
        default:
            assert false : "Unknown protocol: " + program.getStreamingStrategy().getProtocol();
            return null;
        }
    }

    /**
     * @param configuration the configuration to set
     */
    public void setConfiguration(Configuration configuration) {
        this.configuration = configuration;
    }

}
