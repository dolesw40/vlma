/*
 * Copyright (C) 2006-2009 the VideoLAN team
 *
 * This file is part of VLMa.
 *
 * VLMa is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * VLMa is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VLMa. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.videolan.vlma.order.management;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import org.apache.log4j.Logger;
import org.videolan.vlma.dao.VLMaDao;
import org.videolan.vlma.model.Adapter;
import org.videolan.vlma.model.AdapterFamily;
import org.videolan.vlma.model.DTTChannel;
import org.videolan.vlma.model.DTTProgramGroup;
import org.videolan.vlma.model.FilesChannel;
import org.videolan.vlma.model.FilesProgramGroup;
import org.videolan.vlma.model.Media;
import org.videolan.vlma.model.Order;
import org.videolan.vlma.model.Program;
import org.videolan.vlma.model.ProgramGroup;
import org.videolan.vlma.model.SatChannel;
import org.videolan.vlma.model.SatProgramGroup;
import org.videolan.vlma.model.Server;
import org.videolan.vlma.model.StreamChannel;
import org.videolan.vlma.model.StreamProgramGroup;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ListMultimap;
import com.google.common.collect.Multimap;

/**
 * This class computes the orders to give to the servers depending on the
 * medias programmation, on the state of servers state, and on the priority
 * of the programs.
 * <p>
 * To assign medias to servers, this class performs the following steps:
 * <ol>
 *   <li>A partition of available {@link Adapter}s is built according to their
 *   types.</li>
 *   <li>{@link Media}s are grouped together in {@link ProgramGroup}s and
 *   partitioned according to the type of the {@link Adapter}s than can
 *   stream them.</li>
 *   <li>{@link MediaGroup}s are sorted by
 *   {@link Program#getPriority() priority} and {@link Order}s are given to
 *   {@link Adapter}s as long as there are available ones.</li>
 * </ol>
 * This means that the {@link ProgramGroup}s with a low priority may not be
 * streamed.
 *
 * @author Adrien Maglo <magsoft at videolan.org>
 * @author Sylvain Cadilhac <sylv at videolan.org>
 * @author Adrien Grand <jpountz at videolan.org>
 */
public class OrderComputer {

    private static final Logger logger = Logger.getLogger(OrderComputer.class);

    /**
     * Expected number of {@link AdapterFamily}s.
     */
    private static final int EXPECTED_NUMBER_OF_ADAPTER_FAMILIES = 7;

    /**
     * Expected number of {@link Adapter}s per {@link AdapterFamily}.
     */
    private static final int EXPECTED_NUMBER_OF_ADAPTERS = 50;

    private VLMaDao vlmaDao;

    /**
     * Sets the VLMa service.
     *
     * @param vlmaDao the vlmaDao to set
     */
    public void setVlmaDao(VLMaDao vlmaDao) {
        this.vlmaDao = vlmaDao;
    }

    /**
     * Partition adapters according to their type.
     *
     * @param servers available servers
     * @return the partition of adapters
     */
    public ListMultimap<AdapterFamily, Adapter> partitionAdapters(Collection<Server> servers) {

        int nbAdapters = 0;
        ListMultimap<AdapterFamily, Adapter> adapters =
            ArrayListMultimap.create(EXPECTED_NUMBER_OF_ADAPTER_FAMILIES,
                    EXPECTED_NUMBER_OF_ADAPTERS);

        for(Server server : servers) {
            if (!server.isUp()) {
                logger.debug("Skipping server " + server.getName() + ": down");
                continue;
            }
            for(Adapter adapter : server.getAdapters()) {
                if (!adapter.isUp()) {
                    logger.debug("Skipping adapter " + adapter.getName() + " of " + adapter.getServer().getName() + ": down");
                    continue;
                }
                ++nbAdapters;
                adapters.put(adapter.getFamily(), adapter);
            }
        }
        logger.debug(nbAdapters + " available adapters");
        return adapters;
    }

    /**
     * Partition medias according to the type of the adapters that can
     * stream them.
     *
     * @param medias medias to partition
     * @param adapters available adapters
     * @return the partition of media groups
     */
    public ListMultimap<AdapterFamily, ProgramGroup> partitionMedias(Collection<Media> medias, Multimap<AdapterFamily, Adapter> adapters) {
        int nbGroups = 0;
        int nbPrograms = 0;
        ListMultimap<AdapterFamily, ProgramGroup> groups =
            ArrayListMultimap.create(EXPECTED_NUMBER_OF_ADAPTER_FAMILIES,
                    EXPECTED_NUMBER_OF_ADAPTERS);

        for(Media media : medias) {
            boolean adapterGroupFound = media.getPrograms().isEmpty();
            for (Program program : media.getPrograms()) {
                if (!program.isTimeToPlay()) {
                    logger.debug("Skipping " + program.getName() + ": not time to play");
                    continue;
                }
                nbPrograms++;

                Set<AdapterFamily> families = adapters.keySet();
                for(AdapterFamily family : families) {
                    Collection<Adapter> sameFamilyAdapters = adapters.get(family);

                    if (!(sameFamilyAdapters.iterator().next().canRead(media))) {
                        continue;
                    }

                    adapterGroupFound = true;
                    Collection<ProgramGroup> sameTypeGroups = groups.get(family);
                    boolean programAdded = false;
                    for(ProgramGroup group : sameTypeGroups) {
                        if (program.belongsToGroup(group)) {
                            group.add(program);
                            programAdded = true;
                            break;
                        }
                    }

                    if (!programAdded) {
                        // The program does not belong to any existing group
                        // so let's create a new one.
                        ProgramGroup group;
                        if (media instanceof SatChannel) {
                            group = new SatProgramGroup();
                        } else if (media instanceof DTTChannel) {
                            group = new DTTProgramGroup();
                        } else if (media instanceof FilesChannel) {
                            group = new FilesProgramGroup();
                        } else if (media instanceof StreamChannel) {
                            group = new StreamProgramGroup();
                        } else {
                            group = null;
                            assert false : "Unknown media class";
                        }
                        ++nbGroups;
                        group.add(program);
                        sameTypeGroups.add(group);
                    }

                }
            }
            if (!adapterGroupFound) {
                logger.warn("No adapter available for reading: " + media.getName());
            }
        }
        if (logger.isDebugEnabled()) {
            logger.debug(Integer.toString(nbPrograms) + " programs divided into " + nbGroups + " groups");
        }
        return groups;
    }

    /**
     * Compute the list of orders that have to be sent in order the provided
     * ProgramGroup to be streamed using provided adapters.
     *
     * @param groups the media groups to be streamed
     * @param adapters available adapters
     * @return the set of corresponding orders
     */
    public Set<Order> computeOrders(ListMultimap<AdapterFamily, ProgramGroup> groups,
            ListMultimap<AdapterFamily, Adapter> adapters) {

        Map<Server, Integer> load = new HashMap<Server, Integer>();
        for(Adapter adapter : adapters.values()) {
            load.put(adapter.getServer(), 0);
        }

        // Storage backend for orders
        SortedSet<Order> orders = new TreeSet<Order>();

        // First give orders to stream Satellite and DTT channels
        for (AdapterFamily family : groups.keySet()) {
            if (isSatOrDTT(family)) {
                logger.debug("Now attributing adapter family " + family);
                computeOrdersAvailableAdapters(groups.get(family),
                        adapters.get(family),
                        orders,
                        load);
            }
        }

        // Then give other orders (for which there is not a limit of one
        // ProgramGroup per adapter) to servers with lower load
        for (AdapterFamily family : groups.keySet()) {
            if (!isSatOrDTT(family)) {
                logger.debug("Now attributing adapter family " + family);
                computeOrdersLowerLoad(groups.get(family),
                        orders,
                        load);
            }
        }

        return orders;
    }

    private boolean isSatOrDTT(AdapterFamily family) {
        return family instanceof AdapterFamily.Sat || family instanceof AdapterFamily.DTT;
    }

    static Comparator<Adapter> higherScoreFirst = new Comparator<Adapter>() {
        public int compare(Adapter a0, Adapter a1) {
            return a1.getScore() - a0.getScore();
        }
    };

    private void computeOrdersAvailableAdapters(List<ProgramGroup> groups,
            List<Adapter> adapters, SortedSet<Order> orders, final Map<Server, Integer> load) {
        // Handle groups with higher priority first
        Collections.sort(groups);
        // Give adapters with the highest score to groups with the highest score
        Collections.sort(adapters, higherScoreFirst);
        Iterator<ProgramGroup> groupIt = groups.iterator();
        Iterator<Adapter> adapterIt = adapters.iterator();
        while(groupIt.hasNext() && adapterIt.hasNext()) {
            Adapter a = adapterIt.next();
            ProgramGroup g = groupIt.next();
            Order order = new Order(a, g);
            logger.debug("Attributing " + g + " (priority " + g.getPriority() + ") to " + a.getName() + " of " + a.getServer().getName() + "(score " + a.getScore() + ")");
            if(!orders.add(order)) {
                logger.error("Unexpected error while building the new set of orders");
            }
            load.put(a.getServer(), load.get(a.getServer()) + g.getLoad());
        }
        while(groupIt.hasNext()) {
            logger.warn("The following medias cannot be streamed because there is not enough adapters:");
            for(Program program : groupIt.next()) {
                logger.warn(" - " + program.getName());
            }
        }
        // Now that we have a configuration that optimizes the number of
        // broadcasted channels, try to do some basic load balancing
        List<Server> servers = new ArrayList<Server>();
        servers.addAll(load.keySet());
        while(adapterIt.hasNext()) {
            boolean orderFound = false;
            Collections.sort(servers,
                    new Comparator<Server>() {
                        public int compare(Server server1, Server server2) {
                            return load.get(server2) - load.get(server1);
                        }
            });
            Adapter adapter = adapterIt.next();
            for(Server server : servers) {
                if(adapter.getServer().equals(server)) {
                    // This server already has a high load, don't assign orders to this adapter
                    break;
                }
                Iterator<Order> orderIt = orders.iterator();
                while(orderIt.hasNext()) {
                    // orders are already sorted according to their load
                    Order order = orderIt.next();
                    if(order.getAdapter().getServer().equals(server)
                            && adapter.isSuitableFor(order.getPrograms())
                            && order.getPrograms().size() > 1) {
                        /* We found a server with a high load which has to
                         * broadcast medias that could be streamed by an
                         * adapter which is still free and whose server
                         * has a lower load, so change the adapter in charge
                         * of the order */
                        int l = order.getPrograms().getLoad();
                        load.put(order.getAdapter().getServer(), load.get(order.getAdapter().getServer()) - l);
                        order.setAdapter(adapter);
                        load.put(adapter.getServer(), load.get(adapter.getServer()) + l);
                        orderFound = true;
                        break;
                    }
                }
                if(orderFound)
                    break;
            }
        }
    }

    private void computeOrdersLowerLoad(List<ProgramGroup> groups, Set<Order> orders, Map<Server, Integer> load) {
        for(ProgramGroup group : groups) {
            Adapter bestAdapter = null;
            int nbMediasBestAdapter = Integer.MAX_VALUE;
            for(Map.Entry<Server, Integer> entry : load.entrySet()) {
                Server server = entry.getKey();
                int nbMedias = entry.getValue();
                for(Adapter adapter : server.getAdapters()) {
                    if(adapter.isSuitableFor(group) && nbMedias < nbMediasBestAdapter) {
                        bestAdapter = adapter;
                        nbMediasBestAdapter = load.get(adapter.getServer());
                    }
                }
            }
            if(bestAdapter != null) {
                Order order = new Order(bestAdapter, group);
                orders.add(order);
                load.put(bestAdapter.getServer(), nbMediasBestAdapter + group.getLoad());
            } else {
                logger.warn("Cannot find any adapter for media group containing the following medias:");
                for(Program program : group) {
                    logger.warn(" - " + program.getName());
                }
            }
        }
    }

    public Set<Order> computeOrders() {
        ListMultimap<AdapterFamily, Adapter> adapters = partitionAdapters(vlmaDao.getServers());
        ListMultimap<AdapterFamily, ProgramGroup> groups = partitionMedias(vlmaDao.getMedias(), adapters);
        return computeOrders(groups, adapters);
    }

}
