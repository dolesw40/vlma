/*
 * Copyright (C) 2008 the VideoLAN team
 *
 * This file is part of VLMa.
 *
 * VLMa is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * VLMa is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VLMa. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.videolan.vlma.order.watcher;

import org.videolan.vlma.model.Program;

/**
 * This implementation tries to call the appropriate stream watcher
 * according to the streaming protocol of the program.
 *
 * @author Adrien Grand <jpountz at videolan.org>
 */
public class StreamWatcherDispatcher implements StreamWatcher {

    private StreamWatcher directMulticastStreamWatcher;
    private StreamWatcher httpStreamWatcher;
    private StreamWatcher rtspStreamWatcher;
    private StreamWatcher mockStreamWatcher;

    public boolean isPlayed(Program program) {
        switch (program.getStreamingStrategy().getProtocol()) {
        case HTTP:
            return httpStreamWatcher.isPlayed(program);
        case UDP_MULTICAST:
            return directMulticastStreamWatcher.isPlayed(program);
        case RTSP:
            return rtspStreamWatcher.isPlayed(program);
        default:
            return mockStreamWatcher.isPlayed(program);
        }
    }

    public void setDirectMulticastStreamWatcher(StreamWatcher directMulticastStreamWatcher) {
        this.directMulticastStreamWatcher = directMulticastStreamWatcher;
    }

    public void setHttpStreamWatcher(StreamWatcher httpStreamWatcher) {
        this.httpStreamWatcher = httpStreamWatcher;
    }

    public void setRtspStreamWatcher(StreamWatcher rtspStreamWatcher) {
        this.rtspStreamWatcher = rtspStreamWatcher;
    }

    public void setMockStreamWatcher(StreamWatcher mockStreamWatcher) {
        this.mockStreamWatcher = mockStreamWatcher;
    }

}
