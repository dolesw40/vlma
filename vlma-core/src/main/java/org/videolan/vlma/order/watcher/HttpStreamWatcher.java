/*
 * Copyright (C) 2006-2008 the VideoLAN team
 *
 * This file is part of VLMa.
 *
 * VLMa is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * VLMa is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VLMa. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.videolan.vlma.order.watcher;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import org.videolan.vlma.model.Program;
import org.videolan.vlma.util.Utils;

/**
 * This class monitors HTTP streams. It should never be used directly. Use
 * {@link StreamWatcherDispatcher} instead.
 *
 * @author Adrien Grand <jpountz at videolan.org>
 */
public class HttpStreamWatcher implements StreamWatcher {

    private Utils utils;

    public boolean isPlayed(Program program) {
        URL url;
        try {
            url = new URL(utils.getUrl(program));
        } catch (MalformedURLException e) {
            return false;
        }
        URLConnection conn;
        try {
            conn = url.openConnection();
            if (conn == null) return false;
            if (conn.getContentType() != null && conn.getContentType().contains("application/octet-stream")) {
                return true;
            } else {
                return false;
            }
        } catch (IOException e) {
            return false;
        }
    }

    /**
     * Sets utils.
     *
     * @param utils the utils to set
     */
    public void setUtils(Utils utils) {
        this.utils = utils;
    }

}
