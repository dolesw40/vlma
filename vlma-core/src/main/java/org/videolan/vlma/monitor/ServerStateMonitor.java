/*
 * Copyright (C) 2006-2009 the VideoLAN team
 *
 * This file is part of VLMa.
 *
 * VLMa is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * VLMa is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VLMa. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.videolan.vlma.monitor;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.configuration.Configuration;
import org.apache.log4j.Logger;
import org.videolan.vlma.dao.VLMaDao;
import org.videolan.vlma.model.Server;
import org.videolan.vlma.notifier.Notifier;
import org.videolan.vlma.server.VlcChecker;
import org.videolan.vlma.server.rpc.DataRetriever;

/**
 * This class is the daemon which monitores the servers' state. It periodically
 * tries to connect to the telnet interface to check whether VLC is up and
 * running.
 *
 * @author Sylvain Cadilhac <sylv at videolan.org>
 * @author Adrien Grand <jpountz at videolan.org>
 */
public class ServerStateMonitor extends Monitor {

    private static final Logger logger = Logger.getLogger(ServerStateMonitor.class);

    private Configuration configuration;
    private VLMaDao vlmaDao;
    private Thread serverStateMonitor;
    private Runnable serverStateMonitorRunnable;
    private VlcChecker vlcChecker;
    private DataRetriever dataRetriever;

    private volatile boolean shouldRun;

    public ServerStateMonitor() {
        serverStateMonitorRunnable = new Runnable() {
            public void run() {
                checkVLCs();
                shouldRun = true;
                while (shouldRun) {
                    try {
                        // Wait before looping
                        Thread.sleep(1000L * configuration.getLong("vlma.monitor.server.interval"));
                    } catch (InterruptedException e) {
                        continue;
                    }
                    checkVLCs();
                }
                logger.debug("ServerStateMonitor thread stopped.");
            }
        };
        registerMonitor(this);
    }

    public synchronized boolean isRunning() {
        return (serverStateMonitor != null)
                && (serverStateMonitor.isAlive());
    }

    /**
     * This thread try to connect to the VLCs of all servers to check if they
     * run correctly or not.
     */
    public void checkVLCs() {
        List<Thread> checkThreads = new ArrayList<Thread>();
        for (Server server : vlmaDao.getServers()) {
            Thread checkThread = new CheckThread(server);
            checkThread.setName("VLC checker - " + server.getName());
            checkThreads.add(checkThread);
            checkThread.start();
        }
        for(Thread checkThread : checkThreads) {
            try {
                checkThread.join();
            } catch (InterruptedException e) {
                break;
            }
        }
    }

    public synchronized void start() {
        if (!isRunning()) {
            logger.info("Starting " + this.getClass().getSimpleName());
            serverStateMonitor = new Thread(MonitorThreadGroup.getInstance(), serverStateMonitorRunnable);
            serverStateMonitor.setName("Server State Monitor");
            serverStateMonitor.start();
        }
    }

    public synchronized void stop() {
        logger.info("Stopping " + this.getClass().getSimpleName());
        shouldRun = false;
        serverStateMonitor.interrupt();
    }

    /**
     * Sets the VLMa service.
     *
     * @param VLMaDao the VLMaDao to set
     */
    public void setVlmaDao(VLMaDao VLMaDao) {
        this.vlmaDao = VLMaDao;
    }

    /**
     * Sets the configuration.
     *
     * @param configuration the configuration to set
     */
    public void setConfiguration(Configuration configuration) {
        this.configuration = configuration;
    }

    /**
     * Sets the VLC checker.
     *
     * @param vlcChecker the vlcChecker to set
     */
    public void setVlcChecker(VlcChecker vlcChecker) {
        this.vlcChecker = vlcChecker;
    }

    /**
     * Sets VLC remote controller.
     *
     * @param dataRetriever the dataRetriever to set
     */
    public void setDataRetriever(DataRetriever dataRetriever) {
        this.dataRetriever = dataRetriever;
    }

    private class CheckThread extends Thread {

        private Server server;

        public CheckThread(Server server) {
            this.setDaemon(true);
            this.server = server;
        }

        public void run() {
            synchronized(server) {
                boolean formerState = server.isUp();
                boolean newState = vlcChecker.checkVLC(server);
                if (newState != formerState) {
                    Monitor.dispatch(newState ? Monitor.ServerEvent.UP : Monitor.ServerEvent.DOWN, server);
                } else if (!newState) {
                    // VLC's telnet interface is down for the second consecutive time
                    // Try to restart VLC
                    try {
                        dataRetriever.restartVlc(server);
                    } catch (IOException e) {
                        // Fail, maybe the server is down :'(
                        logger.debug("Could not restart VLC", e);
                    }
                }
            }
        }
    }

    @Override
    protected void onServerDown(Server server) {
        Notifier.dispatchNotification("[WARNING] " + server.getName() + " is down");
    }

    @Override
    protected void onServerUp(Server server) {
        Notifier.dispatchNotification("[INFO] " + server.getName() + " is up");
    }

}
