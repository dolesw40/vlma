/*
 * Copyright (C) 2009 the VideoLAN team
 *
 * This file is part of VLMa.
 *
 * VLMa is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * VLMa is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VLMa. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.videolan.vlma.server.rpc;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * A parser for information retrieved from the web interface of the watchdog.
 *
 * @author Adrien Grand
 */
public class HttpDataReader {

    private BufferedReader reader;

    public HttpDataReader(InputStream is) throws IOException {
        reader = new BufferedReader(new InputStreamReader(is));
        // First line is a boolean telling whether the request was successful or not
        Boolean success = readBoolean();
        if (success == null) {
            throw new IOException("Nothing to read.");
        } else if (!success) {
            throw new IOException(readString());
        }
    }

    public String readString() throws IOException {
        return reader.readLine();
    }

    public Boolean readBoolean() throws IOException {
        String s = readString();
        return s == null ? null : Boolean.valueOf(s);
    }

    public Integer readInt() throws IOException {
        String s = readString();
        return s == null ? null : Integer.valueOf(s);
    }

    public Long readLong() throws IOException {
        String s = readString();
        return s == null ? null : Long.valueOf(s);
    }

    public Double readDouble() throws IOException {
        String s = readString();
        return s == null ? null : Double.valueOf(s);
    }

    public void close() throws IOException {
        reader.close();
    }

}
