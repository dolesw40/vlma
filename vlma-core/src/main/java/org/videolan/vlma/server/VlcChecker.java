/*
 * Copyright (C) 2006-2009 the VideoLAN team
 *
 * This file is part of VLMa.
 *
 * VLMa is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * VLMa is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VLMa. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.videolan.vlma.server;

import java.io.IOException;

import org.apache.commons.configuration.Configuration;
import org.apache.log4j.Logger;
import org.videolan.vlma.model.Server;
import org.videolan.vlma.order.sender.TelnetConnection;

public final class VlcChecker {

    private static final Logger logger = Logger.getLogger(VlcChecker.class);

    private Configuration configuration;

    /**
     * Check the state of a given VLC server and returns its state.
     * The state of the provided server will be updated during this operation.
     *
     * @param server the server to check
     * @return true if up, false otherwise
     */
    public boolean checkVLC(Server server) {
        TelnetConnection conn = new TelnetConnection();
        int port = configuration.getInt("vlc.telnet.port");
        try {
            conn.connect(server.getIp(), port);
            conn.println(configuration.getString("vlc.telnet.password"));
            try {
                conn.waitUntilReady();
                conn.resetIn();
                conn.println("help");
                conn.waitUntilReady();
                int c = conn.read();
                if (c >= 0) {
                    server.setUp(true);
                    logger.trace("Telnet interface of " + server.getName() + " is OK");
                } else {
                    logger.info("Telnet interface of " + server.getName() + " does not reply");
                    server.setUp(false);
                }
            } catch (IOException e) {
                logger.info("Telnet interface of " + server.getName() + " does not give any prompt");
                server.setUp(false);
            }
        } catch (IOException e) {
            logger.info("Unable to contact telnet interface of " + server.getName());
            server.setUp(false);
        } finally {
            try {
                conn.close();
            } catch (IOException e) {
                logger.error(e);
            }
        }
        return server.isUp();
    }

    /**
     * @param configuration the configuration to set
     */
    public void setConfiguration(Configuration configuration) {
        this.configuration = configuration;
    }

}
