# Release script
# Usage: release.sh $VERSION
# Should be replaced by the release plugin as soon as git is supported by the
# maven SCM plugin.

VERSION=$1
POM=pom.xml
POMS=$( find . -name pom.xml )

if [ ! $VERSION ]
then
    echo "Usage:"
    echo "./release.sh <version>"
    exit 1
fi

echo "[INFO] Updating local repository"
git pull --rebase
if [ "$?" -ne 0 ]
then
    echo "[ERROR] Update failed failed."
    exit 1
fi
echo "[INFO] Done."

echo "[INFO] Updating version number..."
# Assuming there are no snapshot dependencies except the VLMa, what should
# always be the case.
sed -i -e "s/<version>.*-SNAPSHOT<\/version>/<version>$VERSION<\/version>/g" $POMS
echo "[INFO] Done."

echo "[INFO] Packaging"
mvn clean package
if [ "$?" -ne 0 ]
then
    echo "[ERROR] Package generation failed."
    exit 1
fi
echo "[INFO] Done."

echo "[INFO] Now check everything is OK and run:"
echo "[INFO] git tag -a -m \"VLMa Release $VERSION\" $VERSION"
echo "[INFO] git push origin $VERSION"
echo "[INFO] mvn site-deploy"
echo "[INFO] Then upload jars."

exit 0
