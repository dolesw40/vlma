/*
 * Copyright (C) 2006-2009 the VideoLAN team
 *
 * This file is part of VLMa.
 *
 * VLMa is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * VLMa is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VLMa. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.videolan.vlma.daemon;

import java.io.File;
import java.net.URL;
import java.rmi.RemoteException;

import org.apache.commons.configuration.BaseConfiguration;
import org.apache.commons.configuration.CombinedConfiguration;
import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.configuration.reloading.FileChangedReloadingStrategy;
import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.videolan.vlma.Data;


/**
 * The VLMa daemon launcher. This class contains the boot process of VLMA
 * software.
 *
 * @see org.videolan.vlma.daemon.Daemon
 * @author Sylvain Cadilhac <sylv at videolan.org>
 */
public class VLMad {

    private static final Logger logger = Logger.getLogger(VLMad.class);

    /**
     * VLMa configuration file.
     */
    public static final String CONFIGURATION_FILE = "config.properties";

    public static final String SATCODX_CONFIGURATION_FILE = "satcodx.properties";

    public static enum Command {
        start,
        reload,
        stop
    }

    public static void usage() {
        System.out.println("Usage: java org.videolan.vlma.daemon.VLMad (start|reload|stop)");
    }

    public void dispatch(Command command) throws RemoteException {
        if(command.equals(Command.start)) {
            logger.debug("Starting VLMad");
            logger.debug("Loading Spring application context");
            ApplicationContext ac = new ClassPathXmlApplicationContext("daemon.xml");
            Daemon daemon = (Daemon) ac.getBean("daemon");
            logger.debug("Registering shutdown hook");
            Runtime.getRuntime().addShutdownHook(new ShutdownHook(daemon));
            daemon.start();
        } else {
            ConfigurableApplicationContext ac = new ClassPathXmlApplicationContext("client.xml");
            Data data = (Data) ac.getBean("dataImporter");
            if(command.equals(Command.stop)) {
                data.stop();
            } else if (command.equals(Command.reload)) {
                data.reload();
            }
        }
    }

    public static void main(String[] args) {
        Command command = null;
        if(args.length == 0) {
            command = Command.start;
        } else {
            try {
                command = Command.valueOf(args[0]);
            } catch(IllegalArgumentException e) {
                System.out.println(args[0] + " is not a valid command.");
                usage();
                System.exit(1);
            }
        }
        VLMad vlmad = new VLMad();
        try {
            vlmad.dispatch(command);
        } catch (RemoteException e) {
            System.err.println("[ERROR] Cannot reach VLMa daemon. Please verify that it is currently running.");
            System.exit(1);
        }
    }

    public static Configuration getDefaultConfiguration() throws ConfigurationException {
        URL configUrl = Daemon.class.getResource("/" + CONFIGURATION_FILE);
        PropertiesConfiguration config = new PropertiesConfiguration(configUrl);
        URL satcodxConfigUrl = Daemon.class.getResource("/" + SATCODX_CONFIGURATION_FILE);
        PropertiesConfiguration satcodxConfig = new PropertiesConfiguration(satcodxConfigUrl);
        CombinedConfiguration result = new CombinedConfiguration();
        result.addConfiguration(config);
        result.addConfiguration(satcodxConfig, "satcodx", "satcodx");
        return result;
    }

    private static File getUserConfFile(String configFileName) {
        String confDir = System.getProperty("vlma.conf");
        if (confDir == null) {
            // If vlma.conf is not set, then read VLMa configuration
            // from the user home.
            confDir = System.getProperty("user.home") + File.separator + ".vlma";
        }
        File dir = new File(confDir);
        //Create the directory if necessary
        dir.mkdirs();
        return new File(dir, configFileName);
    }

    private static PropertiesConfiguration getUserConfiguration(File f) throws ConfigurationException {
        PropertiesConfiguration configuration = new PropertiesConfiguration(f);
        configuration.setAutoSave(true);
        // Reload configuration whenever the file changes
        FileChangedReloadingStrategy reloadingStrategy = new FileChangedReloadingStrategy();
        reloadingStrategy.setConfiguration(configuration);
        configuration.setReloadingStrategy(reloadingStrategy);
        return configuration;
    }

    public static Configuration getUserConfiguration() {
        try {
            return getUserConfiguration(getUserConfFile(CONFIGURATION_FILE));
        } catch (ConfigurationException e) {
            logger.warn("Unable to read configuration file. Modifications won't be saved.", e);
            return new BaseConfiguration();
        }
    }

    private static class ShutdownHook extends Thread {

        private Daemon daemon;

        public ShutdownHook(Daemon daemon) {
            this.daemon = daemon;
        }

        public void run() {
            // This if statement is here to prevent the shutdown hook from
            // being called twice if Daemon#stop has already been triggered
            if (daemon.isRunning())
                daemon.stop();
        }
    }

}
