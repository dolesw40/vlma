/*
 * Copyright (C) 2008 the VideoLAN team
 *
 * This file is part of VLMa.
 *
 * VLMa is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * VLMa is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VLMa. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.videolan.vlma.web.controller.configuration;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.servlet.view.RedirectView;
import org.videolan.vlma.Data;
import org.videolan.vlma.model.StreamingStrategy;

public class StreamingConfigurationController extends SimpleFormController {

    private Data data;

    public void setData(Data data) {
        this.data = data;
    }

    @Override
    public ModelAndView onSubmit(Object command) throws Exception {
        StreamingConfiguration form = (StreamingConfiguration) command;
        data.setProperty("vlc.telnet.port", form.getVlc_telnet_port());
        data.setProperty("vlc.telnet.password", form.getVlc_telnet_password());
        data.setProperty("vlc.monitor.impl", form.getVlc_monitor_impl());
        data.setProperty("vlc.snmp.community", form.getVlc_snmp_community());
        data.setProperty("vlc.snmp.oid.cpu_load", form.getVlc_snmp_oid_cpu_load());
        data.setProperty("vlc.snmp.oid.vlc_cpu", form.getVlc_snmp_oid_vlc_cpu());
        data.setProperty("vlc.snmp.oid.vlc_mem", form.getVlc_snmp_oid_vlc_mem());
        data.setProperty("vlc.snmp.oid.traffic_in", form.getVlc_snmp_oid_traffic_in());
        data.setProperty("vlc.snmp.oid.traffic_out", form.getVlc_snmp_oid_traffic_out());
        data.setProperty("vlc.snmp.oid.vlc_restart", form.getVlc_snmp_oid_vlc_restart());
        data.setProperty("vlc.snmp.oid.vlc_version", form.getVlc_snmp_oid_vlc_version());
        data.setProperty("vlc.snmp.oid.vlc_logtail", form.getVlc_snmp_oid_vlc_logtail());
        data.setProperty("vlc.snmp.oid.vlc_uptime", form.getVlc_snmp_oid_vlc_uptime());
        data.setProperty("vlc.monitor.http.login", form.getVlc_monitor_http_login());
        data.setProperty("vlc.monitor.http.password", form.getVlc_monitor_http_password());
        data.setProperty("vlc.monitor.http.port", form.getVlc_monitor_http_port());
        data.setProperty("vlma.streaming", form.getVlma_streaming());
        data.setProperty("vlma.encapsulation", form.getVlma_encapsulation());
        data.setProperty("vlma.streaming.http.port", form.getVlma_streaming_http_port());
        data.setProperty("vlma.streaming.udp.ipbank.min", form.getVlma_streaming_udp_ipbank_min());
        data.setProperty("vlma.streaming.udp.ipbank.max", form.getVlma_streaming_udp_ipbank_max());
        data.setProperty("vlc.stream.ttl", form.getVlc_stream_ttl().toString());
        data.setProperty("vlc.stream.dvb-bandwidth", form.getVlc_stream_dvb_bandwidth());
        return new ModelAndView(new RedirectView(getSuccessView()));
    }

    @Override
    protected Object formBackingObject(HttpServletRequest request) throws Exception {
        StreamingConfiguration form = new StreamingConfiguration();
        form.setVlc_telnet_port(data.getInt("vlc.telnet.port"));
        form.setVlc_telnet_password(data.getString("vlc.telnet.password"));
        form.setVlc_monitor_impl(data.getString("vlc.monitor.impl"));
        form.setVlc_snmp_community(data.getString("vlc.snmp.community"));
        form.setVlc_snmp_oid_cpu_load(data.getString("vlc.snmp.oid.cpu_load"));
        form.setVlc_snmp_oid_vlc_cpu(data.getString("vlc.snmp.oid.vlc_cpu"));
        form.setVlc_snmp_oid_vlc_mem(data.getString("vlc.snmp.oid.vlc_mem"));
        form.setVlc_snmp_oid_traffic_in(data.getString("vlc.snmp.oid.traffic_in"));
        form.setVlc_snmp_oid_traffic_out(data.getString("vlc.snmp.oid.traffic_out"));
        form.setVlc_snmp_oid_vlc_version(data.getString("vlc.snmp.oid.vlc_version"));
        form.setVlc_snmp_oid_vlc_logtail(data.getString("vlc.snmp.oid.vlc_logtail"));
        form.setVlc_snmp_oid_vlc_restart(data.getString("vlc.snmp.oid.vlc_restart"));
        form.setVlc_snmp_oid_vlc_uptime(data.getString("vlc.snmp.oid.vlc_uptime"));
        form.setVlc_monitor_http_port(data.getInt("vlc.monitor.http.port"));
        form.setVlc_monitor_http_login(data.getString("vlc.monitor.http.login"));
        form.setVlc_monitor_http_password(data.getString("vlc.monitor.http.password"));
        form.setVlma_streaming(data.getString("vlma.streaming"));
        form.setVlma_encapsulation(data.getString("vlma.encapsulation"));
        form.setVlma_streaming_http_port(data.getInt("vlma.streaming.http.port"));
        form.setVlma_streaming_udp_ipbank_min(data.getString("vlma.streaming.udp.ipbank.min"));
        form.setVlma_streaming_udp_ipbank_max(data.getString("vlma.streaming.udp.ipbank.max"));
        form.setVlc_stream_ttl(Integer.valueOf(data.getInt("vlc.stream.ttl")));
        form.setVlc_stream_dvb_bandwidth(data.getInt("vlc.stream.dvb-bandwidth"));
        return form;
    }

    @SuppressWarnings("unchecked")
    @Override
    protected ModelAndView showForm(HttpServletRequest arg0, HttpServletResponse arg1,
            BindException arg2, Map arg3) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        m.put("encapsulations", StreamingStrategy.Encapsulation.values());
        m.put("protocols", StreamingStrategy.Protocol.values());
        m.put("monitors", StreamingConfiguration.monitors);
        return super.showForm(arg0, arg1, arg2, m);
    }
}
