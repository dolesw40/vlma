/*
 * Copyright (C) 2006-2008 the VideoLAN team
 *
 * This file is part of VLMa.
 *
 * VLMa is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * VLMa is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VLMa. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.videolan.vlma.web.controller.media.dtt;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.servlet.view.RedirectView;
import org.videolan.vlma.Data;
import org.videolan.vlma.model.DTTChannel;

public class DTTChannelAddController extends SimpleFormController {

    private Data data;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    @Override
    public ModelAndView onSubmit(Object command) throws Exception {
        DTTChannel nvDTTChannel = new DTTChannel();
        nvDTTChannel.setName(((DTTChannelAdd) command).getName());
        nvDTTChannel.setFrequency(((DTTChannelAdd) command).getFrequency());
        nvDTTChannel.setSid(((DTTChannelAdd) command).getSid());
        data.add(nvDTTChannel);
        return new ModelAndView(new RedirectView(getSuccessView()));
    }

    @Override
    protected Object formBackingObject(HttpServletRequest request)
            throws ServletException {
        DTTChannelAdd dTTChannelAdd = new DTTChannelAdd();
        dTTChannelAdd.setName("");
        dTTChannelAdd.setId(null);
        return dTTChannelAdd;
    }

    @Override
    protected ModelAndView handleRequestInternal(HttpServletRequest arg0,
            HttpServletResponse arg1) throws Exception {
        return super.handleRequestInternal(arg0, arg1);
    }

}
