/*
 * Copyright (C) 2006-2008 the VideoLAN team
 *
 * This file is part of VLMa.
 *
 * VLMa is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * VLMa is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VLMa. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.videolan.vlma.web.controller.server;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.videolan.vlma.Data;
import org.videolan.vlma.model.Adapter;
import org.videolan.vlma.model.Server;

public class ServerAdapterRemoveController extends SimpleFormController {

    private Data data;

    public void setData(Data data) {
        this.data = data;
    }

    @Override
    public ModelAndView onSubmit(Object command) throws Exception {
        int serverId = ((ServerAdapterAdd) command).getServer();
        String name = ((ServerAdapterAdd) command).getName();
        Server server = data.getServer(serverId);
        Iterator<Adapter> it = server.getAdapters().iterator();
        while (it.hasNext()) {
            Adapter a = it.next();
            if (a.getName().equals(name))
                it.remove();
        }
        data.update(server);
        return new ModelAndView(getSuccessView(), "server", server.getId());
    }

    @Override
    protected Object formBackingObject(HttpServletRequest request)
            throws ServletException {
        ServerAdapterAdd serversAdapterRemove = new ServerAdapterAdd();
        int server = Integer.parseInt(request.getParameter("server"));
        String name = request.getParameter("adapter");
        serversAdapterRemove.setServer(server);
        serversAdapterRemove.setName(name);
        return serversAdapterRemove;
    }

    @SuppressWarnings("unchecked")
    @Override
    protected ModelAndView showForm(HttpServletRequest arg0,
            HttpServletResponse arg1, BindException arg2, Map arg3)
            throws Exception {
        int serverId = Integer.parseInt(arg0.getParameter("server"));
        Server server = data.getServer(serverId);
        Adapter adapter = null;
        for (Adapter a : server.getAdapters()) {
            if (a.getName().equals(arg0.getParameter("adapter"))) {
                adapter = a;
            }
        }
        Map<String, Object> m = new HashMap<String, Object>();
        m.put("server", server);
        m.put("adapter", adapter);
        return super.showForm(arg0, arg1, arg2, m);
    }

}
