/*
 * Copyright (C) 2006-2008 the VideoLAN team
 *
 * This file is part of VLMa.
 *
 * VLMa is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * VLMa is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VLMa. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.videolan.vlma.web.controller.server;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.videolan.vlma.Data;
import org.videolan.vlma.exception.AlreadyExistsException;
import org.videolan.vlma.model.Adapter;
import org.videolan.vlma.model.DVBSAdapter;
import org.videolan.vlma.model.Server;

public class ServerAdapterAddController extends SimpleFormController {

    private Data data;

    public void setData(Data data) {
        this.data = data;
    }

    @SuppressWarnings("unchecked")
    @Override
    public ModelAndView onSubmit(Object command) throws Exception {
        String adapterName = ((ServerAdapterAdd) command).getName();
        int serverId = ((ServerAdapterAdd) command).getServer();
        Server server = data.getServer(serverId);
        for (Adapter adapter : server.getAdapters()) {
            if (adapter.getName().equals(adapterName))
                throw new AlreadyExistsException("Server " + server.getName() + " already has an adapter whose name is " + adapterName);
        }
        String type = ((ServerAdapterAdd) command).getType();
        Class<Adapter> forName = (Class<Adapter>) Class.forName(type);
        Class<Adapter> clazz = forName;
        Adapter a = clazz.newInstance();
        a.setName(adapterName);
        a.setServer(server);
        server.addAdapter(a);
        if (a instanceof DVBSAdapter) {
            int satelliteId = ((ServerAdapterAdd) command).getSatellite();
            ((DVBSAdapter) a).setSatellite(data.getSatellite(satelliteId));
        }
        data.update(server);
        return new ModelAndView(getSuccessView(), "server", server.getId());
    }

    @Override
    protected Object formBackingObject(HttpServletRequest request)
            throws ServletException {
        ServerAdapterAdd serversAdapterAdd = new ServerAdapterAdd();
        serversAdapterAdd.setName("");
        serversAdapterAdd.setServer(Integer.parseInt(request
                .getParameter("server")));
        serversAdapterAdd.setType("DVB-T");
        serversAdapterAdd.setSatellite(null);
        return serversAdapterAdd;
    }

    @SuppressWarnings("unchecked")
    @Override
    protected ModelAndView showForm(HttpServletRequest arg0,
            HttpServletResponse arg1, BindException arg2, Map arg3)
            throws Exception {
        Server server = data.getServer(Integer.parseInt(arg0
                .getParameter("server")));
        Map<String, Object> m = new HashMap<String, Object>();
        m.put("server", server);
        m.put("satellites", data.getSatellites());
        return super.showForm(arg0, arg1, arg2, m);
    }

}
