/*
 * Copyright (C) 2006-2008 the VideoLAN team
 *
 * This file is part of VLMa.
 *
 * VLMa is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * VLMa is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VLMa. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.videolan.vlma.web.controller.media;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.servlet.view.RedirectView;
import org.videolan.vlma.Data;
import org.videolan.vlma.model.FilesChannel;
import org.videolan.vlma.model.Media;
import org.videolan.vlma.model.Program;
import org.videolan.vlma.model.SatChannel;
import org.videolan.vlma.model.StreamChannel;

public class MediaProgramRemoveController extends SimpleFormController {

    private Data data;

    private String successViewSat;

    private String successViewDTT;

    private String successViewFile;

    private String successViewStream;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public String getSuccessViewSat() {
        return successViewSat;
    }

    public void setSuccessViewSat(String successViewSat) {
        this.successViewSat = successViewSat;
    }

    public String getSuccessViewDTT() {
        return successViewDTT;
    }

    public void setSuccessViewDTT(String successViewDTT) {
        this.successViewDTT = successViewDTT;
    }

    public String getSuccessViewFile() {
        return successViewFile;
    }

    public void setSuccessViewFile(String successViewFile) {
        this.successViewFile = successViewFile;
    }

    public String getSuccessViewStream() {
        return successViewStream;
    }

    public void setSuccessViewStream(String successViewStream) {
        this.successViewStream = successViewStream;
    }

    @Override
    public ModelAndView onSubmit(Object command) throws Exception {
        MediaProgramRemove mediaProgramRemove = (MediaProgramRemove) command;
        int mediaId = mediaProgramRemove.getMediaId();
        String programId = mediaProgramRemove.getProgramId();
        Media media = data.getMedia(mediaId);
        Iterator<Program> it = media.getPrograms().iterator();
        while (it.hasNext()) {
            Program program = it.next();
            if (programId.equals(program.getId())) {
                it.remove();
            }
        }
        data.update(media);

        // Determine the success view with the media class
        String successView = null;
        if (media instanceof FilesChannel) {
            successView = getSuccessViewFile();
        } else if (media instanceof StreamChannel) {
            successView = getSuccessViewStream();
        } else if (media instanceof SatChannel) {
            successView = getSuccessViewSat();
        } else {
            successView = getSuccessViewDTT();
        }

        data.giveOrders();

        return new ModelAndView(new RedirectView(successView));
    }

    @Override
    protected Object formBackingObject(HttpServletRequest request)
            throws ServletException {
        MediaProgramRemove mediasProgramRemove = new MediaProgramRemove();
        int media = Integer.parseInt(request.getParameter("media"));
        String program = request.getParameter("program");
        mediasProgramRemove.setMediaId(media);
        mediasProgramRemove.setProgramId(program);
        return mediasProgramRemove;
    }

    @SuppressWarnings("unchecked")
    @Override
    protected ModelAndView showForm(HttpServletRequest arg0,
            HttpServletResponse arg1, BindException arg2, Map arg3)
            throws Exception {
        int mediaId = Integer.parseInt(arg0.getParameter("media"));
        Map<String, Object> m = new HashMap<String, Object>();
        m.put("media", data.getMedia(mediaId));
        return super.showForm(arg0, arg1, arg2, m);
    }

}
