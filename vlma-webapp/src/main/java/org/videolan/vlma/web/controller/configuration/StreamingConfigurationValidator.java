/*
 * Copyright (C) 2008 the VideoLAN team
 *
 * This file is part of VLMa.
 *
 * VLMa is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * VLMa is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VLMa. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.videolan.vlma.web.controller.configuration;

import java.net.InetAddress;
import java.net.UnknownHostException;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

public class StreamingConfigurationValidator implements Validator {

    @SuppressWarnings("unchecked")
    public boolean supports(Class arg0) {
        return arg0.equals(StreamingConfiguration.class);
    }

    static boolean isHostAddress(String ip) {
        try {
            InetAddress.getByName(ip);
            return true;
        } catch (UnknownHostException e) {
            return false;
        }
    }

    public void validate(Object arg0, Errors arg1) {
        StreamingConfiguration form = (StreamingConfiguration) arg0;
        if(form == null) {
            arg1.reject("configuration.edit.error.not-specified");
        } else {
            if(!isHostAddress(form.getVlma_streaming_udp_ipbank_min())) {
                arg1.rejectValue("vlma_streaming_udp_ipbank_min", "configuration.edit.error.invalidaddress");
            }
            if(!isHostAddress(form.getVlma_streaming_udp_ipbank_max())) {
                arg1.rejectValue("vlma_streaming_udp_ipbank_max", "configuration.edit.error.invalidaddress");
            }
        }
    }
}
