/*
 * Copyright (C) 2008 the VideoLAN team
 *
 * This file is part of VLMa.
 *
 * VLMa is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * VLMa is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VLMa. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.videolan.vlma.web.controller.service;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import org.videolan.vlma.Data;
import org.videolan.vlma.model.AnnouncingStrategy;
import org.videolan.vlma.model.Media;
import org.videolan.vlma.model.Program;

/**
 * Abstract playlist controller.
 *
 * @author Adrien Grand <jpountz at videolan.org>
 */
public abstract class AbstractPlaylistController implements Controller {

    private Data data;

    /**
     * Sets the data.
     *
     * @param data the data to set
     */
    public void setData(Data data) {
        this.data = data;
    }

    private AnnouncingStrategy.Announcement announcement;

    public AbstractPlaylistController(AnnouncingStrategy.Announcement announcement) {
        this.announcement = announcement;
    }

    public ModelAndView handleRequest(HttpServletRequest arg0,
            HttpServletResponse arg1) throws Exception {
        ModelAndView mav = new ModelAndView();
        Map<Program, String> programs = new HashMap<Program, String>();
        for (Media media : data.getMedias()) {
            for (Program program : media.getPrograms()) {
                if (program.getPlayer() != null && program.getAnnouncingStrategy().isEnabled(announcement)) {
                    programs.put(program, data.getUrl(program));
                }
            }
        }
        mav.addObject("programs", programs);
        return mav;
    }

}
