/*
 * Copyright (C) 2006-2008 the VideoLAN team
 *
 * This file is part of VLMa.
 *
 * VLMa is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * VLMa is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VLMa. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.videolan.vlma.web.dwr;

import java.io.IOException;
import java.rmi.RemoteException;

import javax.servlet.ServletException;

import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.videolan.vlma.Data;
import org.videolan.vlma.model.Server;

/**
 * A remote ajax controller.
 *
 * Methods of this class will be exposed through DWR and can be called
 * asynchronously.
 *
 * @author Adrien Grand <jpountz at videolan.org>
 */
public class RemoteController {

    private Data data;

    /**
     * Gets the HTML table containing orders which have been sent to VLM
     * instances.
     *
     * @return the HTML content of the page
     * @throws ServletException
     * @throws IOException
     */
    public String getOrdersCommands() throws ServletException, IOException {
        WebContext wctx = WebContextFactory.get();
        return wctx.forwardToString("/ordercommands.htm");
    }

    /**
     * Send a command to the VLM instance of VLC and return the response.
     *
     * @param serverId  the ID of the server to query
     * @param command   the command to send
     * @return the response from VLC
     * @throws RemoteException
     * @throws IOException
     */
    public String getVlcResponse(int serverId, String command) throws RemoteException, IOException {
        Server server = data.getServer(serverId);
        return data.getVlcResponse(server, command);
    }

    /**
     * @param data the data to set
     */
    public void setData(Data data) {
        this.data = data;
    }

}
