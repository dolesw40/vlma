/*
 * Copyright (C) 2009 the VideoLAN team
 *
 * This file is part of VLMa.
 *
 * VLMa is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * VLMa is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VLMa. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.videolan.vlma.web.controller.media.dtt;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.servlet.view.RedirectView;
import org.videolan.vlma.Data;
import org.videolan.vlma.model.DTTChannel;

public class DTTChannelEditController extends SimpleFormController {

    private Data data;

    public void setData(Data data) {
        this.data = data;
    }

    @Override
    public ModelAndView onSubmit(Object command) throws Exception {
        DTTChannelAdd dttChannelAdd = (DTTChannelAdd)command;
        int id = dttChannelAdd.getId();
        DTTChannel channel = (DTTChannel)data.getMedia(id);
        channel.setName(dttChannelAdd.getName());
        channel.setFrequency(dttChannelAdd.getFrequency());
        channel.setSid(dttChannelAdd.getSid());
        data.update(channel);
        return new ModelAndView(new RedirectView(getSuccessView()));
    }

    @Override
    protected Object formBackingObject(HttpServletRequest request)
            throws Exception {

        int id = Integer.parseInt(request.getParameter("id"));
        DTTChannel media = (DTTChannel)data.getMedia(id);

        DTTChannelAdd dTTChannelAdd = new DTTChannelAdd();
        dTTChannelAdd.setName(media.getName());
        dTTChannelAdd.setId(media.getId());
        dTTChannelAdd.setFrequency(media.getFrequency());
        dTTChannelAdd.setSid(media.getSid());

        return dTTChannelAdd;
    }

    @Override
    protected ModelAndView handleRequestInternal(HttpServletRequest arg0,
            HttpServletResponse arg1) throws Exception {
        return super.handleRequestInternal(arg0, arg1);
    }

}
