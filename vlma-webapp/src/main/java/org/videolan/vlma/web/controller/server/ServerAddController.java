/*
 * Copyright (C) 2006-2008 the VideoLAN team
 *
 * This file is part of VLMa.
 *
 * VLMa is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * VLMa is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VLMa. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.videolan.vlma.web.controller.server;

import java.net.UnknownHostException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.servlet.view.RedirectView;
import org.videolan.vlma.Data;
import org.videolan.vlma.model.Adapter;
import org.videolan.vlma.model.FilesAdapter;
import org.videolan.vlma.model.Server;
import org.videolan.vlma.model.StreamAdapter;

public class ServerAddController extends SimpleFormController {

    private Data data;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    @Override
    public ModelAndView onSubmit(Object command) throws Exception {
        String serverName = ((ServerAdd) command).getName();
        Server server = new Server();
        server.setName(serverName);
        server.setAddress(((ServerAdd) command).getAddress());
        Adapter filesAdapter = new FilesAdapter();
        filesAdapter.setName("files-adapter");
        filesAdapter.setServer(server);
        Adapter streamAdapter = new StreamAdapter();
        streamAdapter.setName("stream-adapter");
        streamAdapter.setServer(server);
        /* Add automatically the file-adapter and the stream-adapter
        when creating a new server. */
        server.addAdapter(filesAdapter);
        server.addAdapter(streamAdapter);
        data.add(server);
        return new ModelAndView(new RedirectView(getSuccessView()));
    }

    @Override
    protected Object formBackingObject(HttpServletRequest request)
            throws ServletException, UnknownHostException {
        ServerAdd serversAdd = new ServerAdd();
        serversAdd.setName("vls");
        serversAdd.setAddress("");
        return serversAdd;
    }

    @Override
    protected ModelAndView handleRequestInternal(HttpServletRequest arg0,
            HttpServletResponse arg1) throws Exception {
        return super.handleRequestInternal(arg0, arg1);
    }

}
