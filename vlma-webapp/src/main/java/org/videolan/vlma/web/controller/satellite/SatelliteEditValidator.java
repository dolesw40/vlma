/*
 * Copyright (C) 2006-2008 the VideoLAN team
 *
 * This file is part of VLMa.
 *
 * VLMa is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * VLMa is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VLMa. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.videolan.vlma.web.controller.satellite;

import java.rmi.RemoteException;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.videolan.vlma.Data;
import org.videolan.vlma.model.Satellite;

public class SatelliteEditValidator implements Validator {

    private Data data;

    @SuppressWarnings("unchecked")
    public boolean supports(Class arg0) {
        return arg0.equals(SatelliteAdd.class);
    }

    public void validate(Object arg0, Errors arg1) {
        SatelliteAdd satellitesAdd = (SatelliteAdd) arg0;

        if (satellitesAdd == null) {
            arg1.rejectValue("coverage", "satellites.edit.error.not-specified");
            return;
        } else {
            Satellite satellite;
            try {
                satellite = data.getSatellite(satellitesAdd.getId());
                if (satellite == null) {
                    arg1.rejectValue("coverage",
                            "satellites.edit.error.nonexisting");
                    return;
                }
            } catch (RemoteException e) {
                arg1.rejectValue("coverage", "error.remote_exception");
            }
        }
    }

    /**
     * @param data the data to set
     */
    public void setData(Data data) {
        this.data = data;
    }

}
