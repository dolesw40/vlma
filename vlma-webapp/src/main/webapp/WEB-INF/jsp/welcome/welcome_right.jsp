<%@ include file="/WEB-INF/jsp/include.jsp" %>

<h1><fmt:message key="welcome.page" /></h1>

<div class="right_right">
<h2><fmt:message key="welcome.servers" /></h2>

    <c:set var="serverCount" value="0" />

    <c:forEach items="${servers}" var="server">
        <c:if test="${!server.up}">
            <c:url value="serverview.htm" var="serverUrl">
                <c:param name="server" value="${server.id}" />
            </c:url>
            <p><a href="${serverUrl}">
            <fmt:message key="welcome.servers.unreachable">
                <fmt:param value="${server.name}" />
            </fmt:message>
            </a></p>
            <c:set var="serverCount" value="${serverCount + 1}" />
        </c:if>
    </c:forEach>
    <c:choose>
        <c:when test="${serverCount == 0}">
            <fmt:message key="welcome.servers.ok" />
        </c:when>
        <c:otherwise>
            <fmt:message key="welcome.servers.warning" />
        </c:otherwise>
    </c:choose>

<h2><fmt:message key="welcome.vacantadapter.resources" /></h2>

    <c:set var="vacantAdapterCount" value="0" />

    <c:set var="serverNameDisplayed" value="false" />
    <c:forEach items="${servers}" var="server">
        <c:if test="${serverNameDisplayed}" >
            </ul>
        </c:if>
        <c:set var="serverNameDisplayed" value="false" />
        <c:if test="${server.up}" >
            <c:forEach items="${server.adapters}" var="adapter">
                <c:if test="${!adapter.busy && fn:contains(adapter.class, 'DVB')}">
                    <c:if test="${!serverNameDisplayed}">
                        <c:url value="serverview.htm" var="serverUrl">
                            <c:param name="server" value="${server.id}" />
                            <c:set var="serverNameDisplayed" value="true" />
                            <c:set var="vacantAdapterCount" value="${vacantAdapterCount + 1}" />
                        </c:url>
                        <a href="${serverUrl}"><b>${server.name}</b></a>
                        <ul>
                    </c:if>
                    <li>
                        <fmt:message key="welcome.vacantadapter.adapter" >
                            <fmt:param value="${adapter.name}" />
                        </fmt:message>
                    </li>
                </c:if>
            </c:forEach>
        </c:if>
    </c:forEach>
    <c:if test="${serverNameDisplayed}" >
        </ul>
    </c:if>

    <c:if test="${vacantAdapterCount == 0}">
        <fmt:message key="welcome.vacantadapter.no" />
    </c:if>
</div>

<h2><fmt:message key="welcome.program" /></h2>

<c:if test="${empty dttPrograms && empty satPrograms && empty streamPrograms && empty filePrograms}">
<p><fmt:message key="medias.empty" /></p>
</c:if>

<c:if test="${not empty dttPrograms}">
<h3><fmt:message key="medias.listDTT" /></h3>

<c:forEach items="${dttPrograms}" var="program">
    <vlma:program program="${program}" />
</c:forEach>

</c:if>

<c:if test="${not empty satPrograms}">
<h3><fmt:message key="medias.fulllistSat" /></h3>

<c:forEach items="${satPrograms}" var="program">
    <vlma:program program="${program}" />
</c:forEach>

</c:if>

<c:if test="${not empty filePrograms}">
<h3><fmt:message key="files.list" /></h3>

<c:forEach items="${filePrograms}" var="program">
    <vlma:program program="${program}" />
</c:forEach>
</c:if>

<c:if test="${not empty  streamPrograms}">
<h3><fmt:message key="streams.list" /></h3>

<c:forEach items="${streamPrograms}" var="program">
    <vlma:program program="${program}" />
</c:forEach>

</c:if>