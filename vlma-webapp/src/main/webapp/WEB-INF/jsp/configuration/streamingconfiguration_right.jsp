<%@ include file="/WEB-INF/jsp/include.jsp" %>

<h1><fmt:message key="configuration.page" /></h1>

<form method="post">

<h2><fmt:message key="configuration.vlc" /></h2>

<h3><fmt:message key="configuration.vlc.telnet" /></h3>

<table class="configuration fullwidth">
    <tr>
        <td><fmt:message key="vlc.telnet.port" /></td>
        <spring:bind path="streamingConfiguration.vlc_telnet_port">
            <td><input name="vlc_telnet_port" type="text" value="<c:out value="${status.value}"/>" /></td>
            <td><span class="error"><c:out value="${status.errorMessage}" /></span></td>
        </spring:bind>
    </tr>
    <tr>
        <td><fmt:message key="vlc.telnet.password" /></td>
        <spring:bind path="streamingConfiguration.vlc_telnet_password">
            <td><input name="vlc_telnet_password" type="text" value="<c:out value="${status.value}"/>" /></td>
            <td><span class="error"><c:out value="${status.errorMessage}" /></span></td>
        </spring:bind>
    </tr>
</table>

<h3><fmt:message key="configuration.vlc.monitor" /></h3>

<table class="configuration fullwidth">
    <tr>
        <td><fmt:message key="vlc.monitor.impl" /></td>
        <spring:bind path="streamingConfiguration.vlc_monitor_impl">
            <td>
                <select id="monitor" name="vlc_monitor_impl" onchange="onMonitorUpdate();">
                <c:forEach items="${monitors}" var="monitor">
                    <option <c:if test="${monitor == status.value}">selected="selected"</c:if>>${monitor}</option>
                </c:forEach>
                </select>
            </td>
            <td><span class="error"><c:out value="${status.errorMessage}" /></span></td>
        </spring:bind>
    </tr>
</table>

<span id="monitor-NONE" />

<table id="monitor-HTTP" class="configuration fullwidth">
    <tr>
        <td><fmt:message key="vlc.monitor.http.login" /></td>
        <spring:bind path="streamingConfiguration.vlc_monitor_http_login">
            <td><input name="vlc_monitor_http_login" type="text" value="<c:out value="${status.value}"/>" /></td>
            <td><span class="error"><c:out value="${status.errorMessage}" /></span></td>
        </spring:bind>
    </tr>
    <tr>
        <td><fmt:message key="vlc.monitor.http.password" /></td>
        <spring:bind path="streamingConfiguration.vlc_monitor_http_password">
            <td><input name="vlc_monitor_http_password" type="text" value="<c:out value="${status.value}"/>" /></td>
            <td><span class="error"><c:out value="${status.errorMessage}" /></span></td>
        </spring:bind>
    </tr>
    <tr>
        <td><fmt:message key="vlc.monitor.http.port" /></td>
        <spring:bind path="streamingConfiguration.vlc_monitor_http_port">
            <td><input name="vlc_monitor_http_port" type="text" value="<c:out value="${status.value}"/>" /></td>
            <td><span class="error"><c:out value="${status.errorMessage}" /></span></td>
        </spring:bind>
    </tr>
</table>

<table id="monitor-SNMP" class="configuration fullwidth">
    <tr>
        <td><fmt:message key="vlc.snmp.community" /></td>
        <spring:bind path="streamingConfiguration.vlc_snmp_community">
            <td><input name="vlc_snmp_community" type="text" value="<c:out value="${status.value}"/>" /></td>
            <td><span class="error"><c:out value="${status.errorMessage}" /></span></td>
        </spring:bind>
    </tr>
    <tr>
        <td><fmt:message key="vlc.snmp.oid.cpu_load" /></td>
        <spring:bind path="streamingConfiguration.vlc_snmp_oid_cpu_load">
            <td><input name="vlc_snmp_oid_cpu_load" type="text" value="<c:out value="${status.value}"/>" /></td>
            <td><span class="error"><c:out value="${status.errorMessage}" /></span></td>
        </spring:bind>
    </tr>
    <tr>
        <td><fmt:message key="vlc.snmp.oid.vlc_cpu" /></td>
        <spring:bind path="streamingConfiguration.vlc_snmp_oid_vlc_cpu">
            <td><input name="vlc_snmp_oid_vlc_cpu" type="text" value="<c:out value="${status.value}"/>" /></td>
            <td><span class="error"><c:out value="${status.errorMessage}" /></span></td>
        </spring:bind>
    </tr>
    <tr>
        <td><fmt:message key="vlc.snmp.oid.vlc_mem" /></td>
        <spring:bind path="streamingConfiguration.vlc_snmp_oid_vlc_mem">
            <td><input name="vlc_snmp_oid_vlc_mem" type="text" value="<c:out value="${status.value}"/>" /></td>
            <td><span class="error"><c:out value="${status.errorMessage}" /></span></td>
        </spring:bind>
     </tr>
     <tr>
        <td><fmt:message key="vlc.snmp.oid.traffic_in" /></td>
        <spring:bind path="streamingConfiguration.vlc_snmp_oid_traffic_in">
            <td><input name="vlc_snmp_oid_traffic_in" type="text" value="<c:out value="${status.value}"/>" /></td>
            <td><span class="error"><c:out value="${status.errorMessage}" /></span></td>
        </spring:bind>
    </tr>
    <tr>
        <td><fmt:message key="vlc.snmp.oid.traffic_out" /></td>
        <spring:bind path="streamingConfiguration.vlc_snmp_oid_traffic_out">
            <td><input name="vlc_snmp_oid_traffic_out" type="text" value="<c:out value="${status.value}"/>" /></td>
            <td><span class="error"><c:out value="${status.errorMessage}" /></span></td>
        </spring:bind>
    </tr>
    <tr>
        <td><fmt:message key="vlc.snmp.oid.vlc_logtail" /></td>
        <spring:bind path="streamingConfiguration.vlc_snmp_oid_vlc_logtail">
            <td><input name="vlc_snmp_oid_vlc_logtail" type="text" value="<c:out value="${status.value}"/>" /></td>
            <td><span class="error"><c:out value="${status.errorMessage}" /></span></td>
        </spring:bind>
    </tr>
    <tr>
        <td><fmt:message key="vlc.snmp.oid.vlc_version" /></td>
        <spring:bind path="streamingConfiguration.vlc_snmp_oid_vlc_version">
            <td><input name="vlc_snmp_oid_vlc_version" type="text" value="<c:out value="${status.value}"/>" /></td>
            <td><span class="error"><c:out value="${status.errorMessage}" /></span></td>
        </spring:bind>
    </tr>
    <tr>
        <td><fmt:message key="vlc.snmp.oid.vlc_restart" /></td>
        <spring:bind path="streamingConfiguration.vlc_snmp_oid_vlc_restart">
            <td><input name="vlc_snmp_oid_vlc_restart" type="text" value="<c:out value="${status.value}"/>" /></td>
            <td><span class="error"><c:out value="${status.errorMessage}" /></span></td>
        </spring:bind>
    </tr>
    <tr>
        <td><fmt:message key="vlc.snmp.oid.vlc_uptime" /></td>
        <spring:bind path="streamingConfiguration.vlc_snmp_oid_vlc_uptime">
            <td><input name="vlc_snmp_oid_vlc_uptime" type="text" value="<c:out value="${status.value}"/>" /></td>
            <td><span class="error"><c:out value="${status.errorMessage}" /></span></td>
        </spring:bind>
    </tr>
</table>

<h2><fmt:message key="configuration.vlma.streaming" /></h2>

<h3><fmt:message key="configuration.vlma.streaming.default" /></h3>

<table class="configuration fullwidth">
    <tr>
        <td><fmt:message key="vlma.streaming" /></td>
        <spring:bind path="streamingConfiguration.vlma_streaming">
            <td>
                <select name="vlma_streaming">
                <c:forEach items="${protocols}" var="protocol">
                    <option <c:if test="${protocol == status.value}">selected="selected"</c:if>>${protocol}</option>
                </c:forEach>
                </select>
            </td>
            <td><span class="error"><c:out value="${status.errorMessage}" /></span></td>
        </spring:bind>
    </tr>
    <tr>
        <td><fmt:message key="vlma.encapsulation" /></td>
        <spring:bind path="streamingConfiguration.vlma_encapsulation">
            <td>
                <select name="vlma_encapsulation">
                <c:forEach items="${encapsulations}" var="encapsulation">
                    <option <c:if test="${encapsulation == status.value}">selected</c:if>>${encapsulation}</option>
                </c:forEach>
                </select>
            </td>
            <td><span class="error"><c:out value="${status.errorMessage}" /></span></td>
        </spring:bind>
    </tr>
</table>

<h3><fmt:message key="configuration.vlma.streaming.misc" /></h3>

<table class="configuration fullwidth">
    <tr>
        <td><fmt:message key="vlma.streaming.http.port" /></td>
        <spring:bind path="streamingConfiguration.vlma_streaming_http_port">
            <td><input name="vlma_streaming_http_port" type="text" value="<c:out value="${status.value}"/>" /></td>
            <td><span class="error"><c:out value="${status.errorMessage}" /></span></td>
        </spring:bind>
    </tr>
    <tr>
        <td><fmt:message key="vlma.streaming.udp.ipbank" /></td>
        <td><fmt:message key="vlma.streaming.udp.ipbank.min" />
        <spring:bind path="streamingConfiguration.vlma_streaming_udp_ipbank_min">
            <input name="vlma_streaming_udp_ipbank_min" type="text" value="<c:out value="${status.value}"/>" /></td>
            <td><span class="error"><c:out value="${status.errorMessage}" /></span></td>
        </spring:bind>
    </tr>
    <tr>
        <td></td>
        <td><fmt:message key="vlma.streaming.udp.ipbank.max" />
        <spring:bind path="streamingConfiguration.vlma_streaming_udp_ipbank_max">
            <input name="vlma_streaming_udp_ipbank_max" type="text" value="<c:out value="${status.value}"/>" /></td>
            <td><span class="error"><c:out value="${status.errorMessage}" /></span></td>
        </spring:bind>
    </tr>
    <tr>
        <td><fmt:message key="vlc.stream.ttl" /></td>
        <spring:bind path="streamingConfiguration.vlc_stream_ttl">
            <td><input name="vlc_stream_ttl" type="text" value="<c:out value="${status.value}"/>" /></td>
            <td><span class="error"><c:out value="${status.errorMessage}" /></span></td>
        </spring:bind>
    </tr>
    <tr>
        <td><fmt:message key="vlc.stream.dvb-bandwidth" /></td>
        <spring:bind path="streamingConfiguration.vlc_stream_dvb_bandwidth">
            <td><input name="vlc_stream_dvb_bandwidth" type="text" value="<c:out value="${status.value}"/>" /></td>
            <td><span class="error"><c:out value="${status.errorMessage}" /></span></td>
        </spring:bind>
    </tr>
</table>

<input type="submit" value="<fmt:message key="configuration.save.button" />" />
</form>

<script type="text/javascript">
function resizeTables() {
    var tables = document.getElementsByClassName("configuration");
    for(var i=0; i<tables.length; i++) {
        resizeTable(tables[i]);
    }
}

function resizeTable(table) {
    var tds = table.getElementsByTagName("td");
    tds[0].style.width="30%";
    tds[1].style.width="50%";
    var inputs = table.getElementsByTagName("input");
    for(var j=0; j<inputs.length; j++) {
        inputs[j].parentNode.style.textAlign = "right";
        inputs[j].style.width = "88%";
    }
    var selects = table.getElementsByTagName("select");
    for(var j=0; j<selects.length; j++) {
        selects[j].parentNode.style.textAlign = "right";
        selects[j].style.width = "88%";
    }
}

function onMonitorUpdate() {
    document.getElementById("monitor-HTTP").style.display = "none";
    document.getElementById("monitor-SNMP").style.display = "none";
    var select = document.getElementById("monitor");
    var monitor = select.options[select.selectedIndex].value;
    if(monitor !=  "NONE") {
        var table = document.getElementById("monitor-" + monitor);
        table.style.display = "table";
    }
    resizeTables();
}

onMonitorUpdate();
</script>