/*
 * Copyright (C) 2008 the VideoLAN team
 *
 * This file is part of VLMa.
 *
 * VLMa is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * VLMa is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VLMa. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.videolan.vlma.model;

import java.io.Serializable;

/**
 * The transcoding strategy. Describes how a media should be transcoded.
 *
 * @author Adrien Grand
 */
public class TranscodingStrategy implements Serializable {

    private static final long serialVersionUID = 1L;

    public static enum VideoCodec {
        MPEG1("MPEG 1", "mp1v"),
        MPEG2("MPEG 2", "mp2v"),
        MPEG4("MPEG 4", "mp4v"),
        H264("H264", "h264");
        private VideoCodec(String name, String shortName) { this.name = name; this.shortName = shortName; }
        private String name;
        private String shortName;
        public String getName() { return name; }
        public String getShortName() { return shortName; }
    }

    public static enum AudioCodec {
        MPEG_AUDIO("MPEG audio", "mpga"),
        AC3("AC3 (A52)", "a52"),
        MP3("MP3", "mp3"),
        MPEG4_AUDIO("MPEG 4 Audio (AAC)", "mp4a");
        private AudioCodec(String name, String shortName) { this.name = name; this.shortName = shortName; }
        private String name;
        private String shortName;
        public String getName() { return name; }
        public String getShortName() { return shortName; }
    }

    public TranscodingStrategy() {
        videoCodec = null;
        videoBitrate = 800;
        scale = 1d;
        audioCodec = null;
        audioBitrate = 128;
        deinterlace = false;
    }

    private VideoCodec videoCodec;
    private int videoBitrate;
    private double scale;
    private boolean deinterlace;
    private AudioCodec audioCodec;
    private int audioBitrate;

    /**
     * @return the audioBitrate
     */
    public int getAudioBitrate() {
        return audioBitrate;
    }

    /**
     * @param audioBitrate the audioBitrate to set
     */
    public void setAudioBitrate(int audioBitrate) {
        this.audioBitrate = audioBitrate;
    }

    /**
     * @return the audioCodec
     */
    public AudioCodec getAudioCodec() {
        return audioCodec;
    }

    /**
     * @param audioCodec the audioCodec to set
     */
    public void setAudioCodec(AudioCodec audioCodec) {
        this.audioCodec = audioCodec;
    }

    /**
     * @return the scale
     */
    public double getScale() {
        return scale;
    }

    /**
     * @param scale the scale to set
     */
    public void setScale(double scale) {
        this.scale = scale;
    }

    /**
     * @return the deinterlace
     */
    public boolean isDeinterlace() {
        return deinterlace;
    }

    /**
     * @param deinterlace the deinterlace to set
     */
    public void setDeinterlace(boolean deinterlace) {
        this.deinterlace = deinterlace;
    }

    /**
     * @return the videoBitrate
     */
    public int getVideoBitrate() {
        return videoBitrate;
    }

    /**
     * @param videoBitrate the videoBitrate to set
     */
    public void setVideoBitrate(int videoBitrate) {
        this.videoBitrate = videoBitrate;
    }

    /**
     * @return the videoCodec
     */
    public VideoCodec getVideoCodec() {
        return videoCodec;
    }

    /**
     * @param videoCodec the videoCodec to set
     */
    public void setVideoCodec(VideoCodec videoCodec) {
        this.videoCodec = videoCodec;
    }

}
