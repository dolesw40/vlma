/*
 * Copyright (C) 2009 the VideoLAN team
 *
 * This file is part of VLMa.
 *
 * VLMa is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * VLMa is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VLMa. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.videolan.vlma.model;

/**
 * A group of programs associated with a DTT channel.
 * Every channel in a DTTProgramGroup has the same frequency.
 *
 * @author Adrien Grand
 */
public class DTTProgramGroup extends DVBProgramGroup {

    private static final long serialVersionUID = 1L;

    public DTTProgramGroup() {
        frequency = -1;
    }

    @Override
    public boolean add(Program program) {
        boolean empty = isEmpty();
        boolean result = super.add(program);
        if (empty) {
            DTTChannel channel = (DTTChannel) program.getMedia();
            frequency = channel.getFrequency();
        }
        return result;
    }

    @Override
    protected boolean isAddable(Program program) {
        if (program.getMedia() instanceof DTTChannel) {
            DTTChannel channel = (DTTChannel) program.getMedia();
            return isEmpty() || channel.getFrequency() == frequency; 
        }
        return false;
    }
}
