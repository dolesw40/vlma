/*
 * Copyright (C) 2006-2008 the VideoLAN team
 *
 * This file is part of VLMa.
 *
 * VLMa is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * VLMa is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VLMa. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.videolan.vlma.model;

/**
 * An order that can be sent to a server.
 *
 * @author Adrien Grand <jpountz at videolan.org>
 * @author Sylvain Cadilhac <sylv at videolan.org>
 */
public class Order implements Comparable<Order> {

    private Adapter adapter;

    private ProgramGroup programs;

    /**
     * Constructs a new Order.
     *
     * @param adapter the adapter to send commands to
     * @param programs the mediagroup that will be streamed when the order will
     *               be sent
     */
    public Order(Adapter adapter, ProgramGroup programs) {
        this.adapter = adapter;
        this.programs = programs;
    }

    /**
     * Constructs a new Order with an empty {@link ProgramGroup }.
     *
     * @param adapter the adapter to send commands to
     */
    public Order(Adapter adapter) {
        this(adapter, null);
    }

    /**
     * Gets the adapter associated with the order.
     *
     * @return the adapter
     */
    public Adapter getAdapter() {
        return adapter;
    }

    /**
     * Sets the adapter associated with the order.
     *
     * @param adapter the adapter to set
     */
    public void setAdapter(Adapter adapter) {
        this.adapter = adapter;
    }

    /**
     * Gets the program group to stream.
     *
     * @return the programs
     */
    public ProgramGroup getPrograms() {
        return programs;
    }

    /**
     * Sets the program group to stream.
     *
     * @param programs the programs to set
     */
    public void setPrograms(ProgramGroup medias) {
        this.programs = medias;
    }

    /**
     * Get the name of the order.
     * Two orders with the same name wannot co-exist in the same VLC instance.
     *
     * @return the order name
     */
    public String getName() {
        if(adapter.getCapacity() <= 1) {
            return "adapter-" + adapter.getName();
        } else {
            // Warning if you want to change this, this name is used in the URI for RTSP streaming
            return programs.iterator().next().getId();
        }
    }

    /**
     * Compute the order id.
     * There can not be two orders with the same id at a given time.
     *
     * @return the order id
     */
    public String getId() {
        if(adapter.getCapacity() <= 1) {
            return adapter.getServer().getName() + "-" + getName();
        } else {
            return getName();
        }
    }

    @Override
    public boolean equals(Object o) {
        if (o == null) return false;
        if (o instanceof Order) {
            Order other = (Order) o;
            return adapter.equals(other.getAdapter()) && programs.equals(other.getPrograms());
        }
        return false;
    }

    @Override
    public int hashCode() {
        return adapter.hashCode() + programs.hashCode();
    }

    public int compareTo(Order other) {
        if(other == null) return 1;
        int result = other.getPrograms().getLoad() - getPrograms().getLoad();
        if(result == 0) {
            // To be consistent with the equals method
            result = adapter.getName().compareTo(other.getAdapter().getName());
            if(result == 0) {
                return adapter.getServer().getName().compareTo(other.getAdapter().getServer().getName());
            } else {
                return result;
            }
        } else {
            return result;
        }
    }
}
