/*
 * Copyright (C) 2009 the VideoLAN team
 *
 * This file is part of VLMa.
 *
 * VLMa is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * VLMa is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VLMa. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.videolan.vlma.model;

public class StreamProgramGroup extends ProgramGroup {

    private static final long serialVersionUID = 1L;

    private StreamChannel media;

    public StreamChannel getMedia() {
        return media;
    }

    @Override
    protected boolean isAddable(Program program) {
        return program.getMedia() instanceof StreamChannel;
    }

    @Override
    public boolean add(Program program) {
        boolean empty = isEmpty();
        boolean result = super.add(program);
        if (empty) {
            media = (StreamChannel)program.getMedia();
        }
        return result;
    }
    
}
