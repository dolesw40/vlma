/*
 * Copyright (C) 2008 the VideoLAN team
 *
 * This file is part of VLMa.
 *
 * VLMa is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * VLMa is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VLMa. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.videolan.vlma.model;

/**
 * A virtual adapter, which means that there is no need for a specific device
 * to read data.
 *
 * @author Adrien Grand
 */
public abstract class VirtualAdapter extends Adapter {

    private static final long serialVersionUID = 1L;

    public int getCapacity() {
        return Integer.MAX_VALUE;
    }

    @Override
    public boolean isSuitableFor(Media media) {
        if (!canRead(media)) {
            return false;
        } else if (media instanceof RemoteInputMedia) {
            RemoteInputMedia m = (RemoteInputMedia) media;
            return m.getServer() == null || m.getServer().equals(this.getServer());
        } else {
            // This block should never be executed
            assert false;
            return false;
        }
    }

}
