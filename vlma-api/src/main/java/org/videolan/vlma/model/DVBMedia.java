package org.videolan.vlma.model;

public abstract class DVBMedia extends Media {

    private static final long serialVersionUID = 1L;

    protected int frequency;
    protected int sid;

    public DVBMedia() {
        super();
    }

    /**
     * Gets the channel frequency.
     *
     * @return the channel frequency (kHz)
     */
    public int getFrequency() {
        return frequency;
    }

    /**
     * Sets the channel frequency.
     *
     * @param frequency
     *            the frequency to set (kHz)
     */
    public void setFrequency(int frequencey) {
        this.frequency = frequencey;
    }

    /**
     * Gets the channel SID.
     *
     * @return the channel SID
     */
    public int getSid() {
        return sid;
    }

    /**
     * Sets the channel SID.
     *
     * @param sid
     *            the sid to set
     */
    public void setSid(int sid) {
        this.sid = sid;
    }

}
