/*
 * Copyright (C) 2008 the VideoLAN team
 *
 * This file is part of VLMa.
 *
 * VLMa is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * VLMa is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VLMa. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.videolan.vlma.model;

import java.io.Serializable;
import java.util.Date;


/**
 * A single command sent to a server running VLC.
 *
 * @author Adrien Grand <jpountz at videolan.org>
 */
public class Command implements Serializable {

    private static final long serialVersionUID = 6501305617407724157L;

    private long date;
    private Server server;
    private String command;
    private String response;

    public Command() {
        this.date = System.currentTimeMillis();
    }

    public Command(Server server, String command, String response) {
        this();
        this.server = server;
        this.command = command;
        this.response = response;
    }

    public String getCommand() {
        return command;
    }

    public void setCommand(String command) {
        this.command = command;
    }

    public Date getDate() {
        return new Date(date);
    }

    public long getDateMillis() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date.getTime();
    }

    public void setDateMillis(long date) {
        this.date = date;
    }

    public Server getServer() {
        return server;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public void setServer(Server server) {
        this.server = server;
    }

}
