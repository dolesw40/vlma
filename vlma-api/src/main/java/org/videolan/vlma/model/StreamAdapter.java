/*
 * Copyright (C) 2006-2008 the VideoLAN team
 *
 * This file is part of VLMa.
 *
 * VLMa is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * VLMa is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VLMa. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.videolan.vlma.model;


/**
 * This class represents a file adapter.
 * In fact, this class is added to a server if it has to broadcast files
 *
 * @author Adrien Maglo <magsoft at videolan.org>
 */
public class StreamAdapter extends VirtualAdapter {

    private static final long serialVersionUID = 2L;

    public AdapterFamily getFamily() {
        return AdapterFamily.Stream.getInstance();
    }

    @Override
    public boolean canRead(Media media) {
        return (media instanceof StreamChannel);
    }

}
