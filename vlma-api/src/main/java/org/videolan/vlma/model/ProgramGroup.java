/*
 * Copyright (C) 2006-2009 the VideoLAN team
 *
 * This file is part of VLMa.
 *
 * VLMa is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * VLMa is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VLMa. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.videolan.vlma.model;

import java.util.AbstractCollection;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


/**
 * A program group. A media group is a set of programs which are all available
 * using the same input (same file, DVB card, etc.).
 *
 * @author Sylvain Cadilhac <sylv at videolan.org>
 * @author Adrien Grand <jpountz at videolan.org>
 */
public abstract class ProgramGroup extends AbstractCollection<Program>
                                   implements Comparable<ProgramGroup> {

    // Only one streaming strategy type is possible for a given program group
    private StreamingStrategy.Type type = StreamingStrategy.Type.BROADCAST;

    // Storage backend for the programs
    private List<Program> programs = new ArrayList<Program>();

    /**
     * Compares groups according to their priority.
     *
     * @param o the object to compare to
     * @return 1 if the group has a higher priority, 0 if both groups have the
     *         same priority and -1 otherwise.
     */
    public int compareTo(ProgramGroup o) {
        int result = o.getPriority().compareTo(getPriority());
        if(result == 0) {
            // To be consistent with equals
            return hashCode() - o.hashCode();
        } else {
            return result;
        }
    }

    /**
     * Compares the media group to the specified object.
     *
     * @return true if the media group are equal; false otherwise.
     */
    @Override
    public boolean equals(Object o) {
    if (o == null || !(o instanceof ProgramGroup)) return false;
        ProgramGroup g = (ProgramGroup) o;
        if (g.size() != this.size())
            return false;
        for (Program program : this) {
            if (!g.contains(program))
                return false;
        }
        return true;
    }

    /**
     * Computes the load of the mediagroup.
     * This number should reflect how much resources it takes to the server
     * to stream this media group.
     *
     * @return the mediagroup load
     */
    public int getLoad() {
        // A constant load (per order) plus how many programs will be streamed
        return 1 + size();
    }

    /**
     * Computes the MediaGroup hashCode.
     *
     * This implementation does not take into account the position of the media
     * in the group.
     */
    @Override
    public int hashCode() {
        int result = 0;
        for(Program program : this) {
            result += program.hashCode();
        }
        return result;
    }

    /**
     * Gives the media group priority. It is the sum of the priorities of all
     * media that belong to this group.
     *
     * @return the group priority
     */
    public Integer getPriority() {
        int p = 0;
        for (Program program : this) {
            p += program.getPriority();
        }
        return p;
    }

    /**
     * Can the provided program be added to this group?
     *
     * @param program the program to check
     * @return true if, and only if the program can be added
     */
    protected abstract boolean isAddable(Program program);

    /**
     * Gets the program group's type.
     *
     * @return the proogram group's type
     */
    public StreamingStrategy.Type getType() {
        return type;
    }

    // Methods of the Collection interface

    @Override
    public boolean add(Program program) {
        if (program.belongsToGroup(this)) {
            if (isEmpty()) {
                type = program.getStreamingStrategy().getType();
            }
            return programs.add(program);
        }
        throw new IllegalArgumentException("The program does not belong to this group");
    }

    @Override
    public Iterator<Program> iterator() {
        return programs.iterator();
    }

    @Override
    public int size() {
        return programs.size();
    }

    @Override
    public String toString() {
        boolean first = true;
        StringBuilder result = new StringBuilder();
        for (Program program : this) {
            if (!first) {
                result.append(", ");
            } else {
                first = false;
            }
            result.append(program.getId());
        }
        return result.toString();
    }

}
