/*
 * Copyright (C) 2006-2008 the VideoLAN team
 *
 * This file is part of VLMa.
 *
 * VLMa is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * VLMa is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VLMa. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.videolan.vlma.model;

import java.io.Serializable;

/**
 * VLMa streaming strategy.
 *
 * @author Adrien Grand <jpountz at videolan.org>
 */
public class StreamingStrategy implements Serializable {

    private static final long serialVersionUID = 2L;

    public static enum Type {
        BROADCAST("Broadcast"),
        VOD("VOD");
        private Type(String name) { this.name = name; }
        private String name;
        public String getName() { return name; }
    }

    public static enum Protocol {
        UDP_MULTICAST("UDP multicast", "udp"),
        HTTP("HTTP", "http"),
        RTSP("RTSP", "rtsp");
        private Protocol(String name, String scheme) { this.name = name; this.scheme = scheme; }
        private String name;
        private String scheme;
        public String getName() { return name; }
        public String getScheme() { return scheme; }
    }

    public static enum Encapsulation {
        TS("MPEG-TS", "ts"),
        PS("MPEG-PS", "ps"),
        OGG("Ogg", "ogg"),
        ASF("ASF/WMV", "asf"),
        RAW("Raw", "raw");
        private Encapsulation(String name, String shortName) { this.name = name; this.shortName = shortName; }
        private String name;
        private String shortName;
        public String getName() { return name; }
        public String getShortName() { return shortName; }
    }

    private static final Type DEFAULT_TYPE = Type.BROADCAST;
    private static final Protocol DEFAULT_PROTOCOL = Protocol.UDP_MULTICAST;
    private static final Encapsulation DEFAULT_ENCAPSULATION = Encapsulation.TS;

    private Type type = DEFAULT_TYPE;
    private Protocol protocol = DEFAULT_PROTOCOL;
    private Encapsulation encapsulation = DEFAULT_ENCAPSULATION;

    public StreamingStrategy() {
        type = Type.BROADCAST;
        protocol = Protocol.UDP_MULTICAST;
        encapsulation = Encapsulation.TS;
    }

    /**
     * Get the type.
     *
     * @return the type
     */
    public Type getType() {
        if(type == null) type = Type.BROADCAST;
        return type;
    }

    /**
     * Sets the type.
     *
     * @param type the type
     */
    public void setType(Type type) {
        this.type = type;
    }

    /**
     * Gets the protocol.
     *
     * @return the protocol
     */
    public Protocol getProtocol() {
        return protocol;
    }

    /**
     * Sets the protocol.
     *
     * @param protocol the protocol to set
     */
    public void setProtocol(Protocol protocol) {
        this.protocol = protocol;
    }

    /**
     * Gets the encapsulation.
     *
     * @return the encapsulation
     */
    public Encapsulation getEncapsulation() {
        return encapsulation;
    }

    /**
     * Sets the encapsulation.
     *
     * @param encapsulation the encapsulation to set
     */
    public void setEncapsulation(Encapsulation encapsulation) {
        this.encapsulation = encapsulation;
    }

    @Override
    public boolean equals(Object o) {
        if (o == null) return false;
        if (o instanceof StreamingStrategy) {
            StreamingStrategy ss = (StreamingStrategy) o;
            return type == ss.getType() &&
                    protocol == ss.getProtocol() &&
                    encapsulation == ss.getEncapsulation();
        }
        return false;
    }

    @Override
    public int hashCode() {
        int result = type.hashCode();
        result += 13*protocol.hashCode();
        result += 13*encapsulation.hashCode();
        return result;
    }

}
