/*
 * Copyright (C) 2006-2009 the VideoLAN team
 *
 * This file is part of VLMa.
 *
 * VLMa is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * VLMa is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VLMa. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.videolan.vlma.model;

import java.io.Serializable;

/**
 * Abstract adapter. Defines methods common to several adapters.
 *
 * @author Sylvain Cadilhac <sylv at videolan.org>
 */
public abstract class Adapter implements Serializable {

    private static final long serialVersionUID = 1L;

    private volatile boolean isUp;

    private volatile boolean isBusy;

    private String name;

    private Server server;

    /**
     * The score of an adapter can be understood as the probability this adapter
     * has to work. An adapter with a high score is proven to work whereas an
     * adapter with a low score probably doesn't.
     */
    private volatile transient int score;

    public Adapter() {
        name = "";
        setUp(true);
    }

    /**
     * Indicates whether a media is readable.
     *
     * @param media the media to test
     * @return the readability of the media
     */
    public abstract boolean canRead(Media media);

    /**
     * Get the family of this adapter.
     *
     * @return the adapter family
     */
    public abstract AdapterFamily getFamily();

    /**
     * Check whether this adapter is suitable for the provided media group.
     *
     * @param group the group to test
     * @return true if and only if the adapter is suitable for the media group
     */
    public boolean isSuitableFor(ProgramGroup group) {
        if (group == null || group.size() == 0) {
            return false;
        } else {
            for (Program program : group) {
                if (!isSuitableFor(program.getMedia())) {
                    return false;
                }
            }
            return true;
        }
    }

    /**
     * Check whether this adapter is suitable for the provided media. On the
     * contrary to canRead, it also checks for user preferences (such as the
     * server which has to stream the media, etc.).
     *
     * @param cls the media to check
     * @return true if this adapter is suitable for the provided media
     */
    public boolean isSuitableFor(Media media) {
        return canRead(media);
    }

    /**
     * Returns the adapter name.
     *
     * @return the adapter name
     */
    public String getName() {
        return name;
    }

    /**
     * Returns the server of this adapter.
     *
     * @return VlServer the server
     */
    public Server getServer() {
        return server;
    }

    /**
     * Returns the adapter's score.
     *
     * @return the score
     */
    public int getScore() {
        return score;
    }

    /**
     * Returns the state of the adapter (up or not).
     *
     * @return the state of the adapter (true if up, false otherwise)
     */
    public boolean isUp() {
        return isUp;
    }

    /**
     * Returns the state of the adapter (busy or not).
     *
     * @return the state of the adapter (true if busy, false otherwise)
     */
    public boolean isBusy() {
        return isBusy;
    }

    /**
     * Sets the adapter name.
     *
     * @param name the new adapter name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Sets the server of this adapter.
     *
     */
    public void setServer(Server server) {
        this.server = server;
    }

    /**
     * Sets the state of the adapter (up or not).
     *
     * @param isUp the new adapter state
     */
    public void setUp(boolean isUp) {
        this.isUp = isUp;
    }

    /**
     * Sets the state of the adapter (busy or not).
     *
     * @param isUp the new adapter state
     */
    public void setBusy(boolean isBusy) {
        this.isBusy = isBusy;
    }

    /**
     * Sets the adapter's score.
     *
     * @param score the score to set
     */
    public void setScore(int score) {
        this.score = score;
    }

    /**
     * Capacity of the adapter: how many orders can reference this adapter.
     *
     * @return the capacity of the adapter
     */
    public abstract int getCapacity();

    @Override
    public boolean equals(Object other) {
        if (other == null || !(other instanceof Adapter)) {
            return false;
        }
        Adapter adapter = (Adapter) other;
        return adapter.getName().equals(getName()) &&
            adapter.getServer().equals(getServer());
    }

    @Override
    public int hashCode() {
        return name.hashCode() + server.hashCode();
    }

}
