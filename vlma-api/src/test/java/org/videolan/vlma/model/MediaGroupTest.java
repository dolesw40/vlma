/*
 * Copyright (C) 2008 the VideoLAN team
 *
 * This file is part of VLMa.
 *
 * VLMa is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * VLMa is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VLMa. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.videolan.vlma.model;

import static org.junit.Assert.*;

import org.junit.Test;

public class MediaGroupTest {

    private ProgramGroup group;
    
    @Test
    public void testEquals() throws Exception {
        DTTChannel media1 = new DTTChannel();
        media1.setName("media1");
        media1.setFrequency(42);
        Program program1 = new Program();
        program1.setMedia(media1);
        program1.setId("pouet");
        program1.setName("pouet");
        media1.addProgram(program1);
        DTTChannel media2 = new DTTChannel();
        media2.setName("media2");
        media2.setFrequency(42);
        Program program2 = new Program();
        program2.setMedia(media2);
        program2.setId("plop");
        program2.setName("plop");
        media2.addProgram(program2);
        group = new DTTProgramGroup();
        group.add(program1);
        group.add(program2);
        ProgramGroup group2 = new DTTProgramGroup();
        group2.add(program2);
        group2.add(program1);
        assertEquals(group, group2);
        assertEquals(group.hashCode(), group2.hashCode());
    }

    @Test
    public void testAdd() {
        group = new FilesProgramGroup();
        Media m1 = new FilesChannel();
        Program p1 = new Program();
        p1.setMedia(m1);
        p1.setId("pouet");
        m1.addProgram(p1);
        group.add(p1);
        try {
            Media m = new FilesChannel();
            Program p = new Program();
            p.setMedia(m);
            p.setId("plop");
            m.addProgram(p);
            group.add(p);
            assertFalse(true);
        } catch(Exception e) {}
    }

    @Test
    public void testAdd2() {
        group = new SatProgramGroup();
        Media m1 = new SatChannel();
        Program p1 = new Program();
        p1.setMedia(m1);
        p1.setId("pouet");
        m1.addProgram(p1);
        group.add(p1);
        try {
            Media m = new DTTChannel();
            Program p = new Program();
            p.setMedia(m);
            p.setName("plop");
            m.addProgram(p);
            group.add(p);
            assertFalse(true);
        } catch(IllegalArgumentException e) {}
    }

    @Test
    public void testAdd3() {
        group = new SatProgramGroup();
        SatChannel m1 = new SatChannel();
        m1.setFrequency(42);
        m1.setSid(12);
        Program p1 = new Program();
        p1.setMedia(m1);
        p1.setId("pouet");
        m1.addProgram(p1);
        group.add(p1);
        try {
            SatChannel m2 = new SatChannel();
            m2.setFrequency(12);
            m2.setSid(42);
            Program p2 = new Program();
            p2.setMedia(m2);
            p2.setName("plop");
            m2.addProgram(p2);
            group.add(p2);
            assertFalse(true);
        } catch(IllegalArgumentException e) {}
    }

}
